
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "config.h"
#include "gui/mainwindow.h"
#include "utils/types.h"
#include <QApplication>
#include <QTimer>
#include <iostream>
#include <pappsomspp/pappsoexception.h>

using namespace std;

int
main(int argc, char *argv[])
{
  QTextStream errorStream(stderr, QIODevice::WriteOnly);
  QApplication app(argc, argv);

  QResource::registerResource("i2masschroq.rcc");

  qRegisterMetaType<TandemRunBatch>("TandemRunBatch");
  qRegisterMetaType<std::vector<pappso::pappso_double>>(
    "std::vector<pappso::pappso_double>");
  qRegisterMetaType<std::size_t>("std::size_t");
  qRegisterMetaType<pappso::PrecisionPtr>("pappso::PrecisionPtr");
  qRegisterMetaType<std::vector<pappso::XicCstSPtr>>(
    "std::vector<pappso::XicCstSPtr>");
  qRegisterMetaType<pappso::PeptideSp>("pappso::PeptideSp");
  qRegisterMetaType<pappso::XicExtractMethod>("pappso::XicExtractMethod");
  qRegisterMetaType<pappso::MassSpectrumCstSPtr>("pappso::MassSpectrumCstSPtr");
  qRegisterMetaType<MsRunSp>("MsRunSp");
  qRegisterMetaType<std::vector<MsRunSp>>("<std::vector<MsRunSp>>");
  qRegisterMetaType<MsRunAlignmentGroupSp>("MsRunAlignmentGroupSp");
  qRegisterMetaType<std::vector<pappso::PeptideNaturalIsotopeAverageSp>>(
    "std::vector<pappso::PeptideNaturalIsotopeAverageSp>");
  qRegisterMetaType<ExportFastaType>("ExportFastaType");
  qRegisterMetaType<MasschroqFileParameters>("MasschroqFileParameters");
  qRegisterMetaType<IdentificationDataSourceSp>("IdentificationDataSourceSp");
  qRegisterMetaType<ContaminantRemovalMode>("ContaminantRemovalMode");
  qRegisterMetaType<PtmMode>("PtmMode");
  qRegisterMetaType<std::vector<XicBox *>>("<std::vector<XicBox *>>");
  qRegisterMetaType<MassChroQRunBatch>("MassChroQRunBatch");
  qRegisterMetaType<std::vector<pappso::XicCoordSPtr>>(
    "std::vector<pappso::XicCoordSPtr>");

  qRegisterMetaType<std::vector<MsRunAlignmentGroupSp>>(
    "std::vector<MsRunAlignmentGroupSp>");
  qRegisterMetaType<McqrExperimentSp>("McqrExperimentSp");

  // qRegisterMetaType<pappso::PeakIonIsotopeMatch>("pappso::PeakIonIsotopeMatch");

  try
    {
      QCoreApplication::setOrganizationName("PAPPSO");
      QCoreApplication::setOrganizationDomain("pappso.inra.fr");
      QCoreApplication::setApplicationName(SOFTWARE_NAME);
      QCoreApplication::setApplicationVersion(i2MassChroQ_VERSION);
      MainWindow window;
      window.show();

      // This code will start the messaging engine in QT and in
      // 10ms it will start the execution in the MainClass.run routine;
      QTimer::singleShot(10, &window, SLOT(run()));

      return app.exec();
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in i2MassChroQ. Don't panic :"
                  << Qt::endl;
      errorStream << error.qwhat() << Qt::endl;
      QMessageBox::warning(app.activeWindow(), "Error !!", error.qwhat());
      app.exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in i2MassChroQ. Don't panic :"
                  << Qt::endl;
      errorStream << error.what() << Qt::endl;
      QMessageBox::warning(app.activeWindow(), "Error !!", error.what());
      app.exit(1);
    }
}
