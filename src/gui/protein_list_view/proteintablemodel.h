
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QAbstractTableModel>
#include "../../core/project.h"


/** \def ProteinListColumn list of available fields to display in protein list
 *
 */

enum class ProteinListColumn : std::int8_t
{
  checked             = 0,  ///< manual checked
  protein_grouping_id = 1,  ///< protein grouping id
  accession           = 2,  ///< accession
  description         = 3,  ///< protein description
  log_evalue          = 4,  ///< log(Evalue)
  evalue              = 5,  ///< Evalue
  spectrum            = 6,  ///< spectrum count
  specific_spectrum   = 7,  ///< specific spectrum count
  sequence            = 8,  ///< unique sequence count
  specific_sequence   = 9,  ///< specific unique sequence
  coverage            = 10, ///< protein coverage
  molecular_weight    = 11, ///< protein molecular weight in Dalton
  pai                 = 12, ///< PAI
  empai               = 13, ///< emPAI
  last                = 14,
};

class ProteinListWindow;

class ProteinTableModel : public QAbstractTableModel
{
  Q_OBJECT
  public:
  ProteinTableModel(ProteinListWindow *p_protein_list_window);
  virtual int
  rowCount(const QModelIndex &parent = QModelIndex()) const override;
  virtual int
  columnCount(const QModelIndex &parent = QModelIndex()) const override;
  virtual QVariant
  headerData(int section, Qt::Orientation orientation, int role) const override;
  virtual QVariant data(const QModelIndex &index,
                        int role = Qt::DisplayRole) const override;

  static const QString getTitle(ProteinListColumn column);
  static const QString getDescription(ProteinListColumn column);
  static const QString getTitle(std::int8_t column);
  static const QString getDescription(std::int8_t column);
  static ProteinListColumn getProteinListColumn(std::int8_t column);


  void setIdentificationGroup(IdentificationGroup *p_identification_group);
  IdentificationGroup *getIdentificationGroup();

  public slots:
  void onProteinDataChanged();

  private:
  static int getColumnWidth(int column);
  void refresh();

  private:
  IdentificationGroup *_p_identification_group = nullptr;
  ProteinListWindow *_p_protein_list_window;
};
