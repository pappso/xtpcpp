
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QAbstractTableModel>
#include <QSortFilterProxyModel>
#include "../widgets/massitemdelegate.h"
#include "../../core/project.h"
#include "peptidetablemodel.h"

class PeptideListWindow;

class PeptideTableModel;

class PeptideTableProxyModel : public QSortFilterProxyModel
{
  Q_OBJECT
  public:
  PeptideTableProxyModel(PeptideListWindow *p_peptide_list_window,
                         PeptideTableModel *peptide_table_model_p);
  virtual ~PeptideTableProxyModel();
  bool filterAcceptsRow(int source_row,
                        const QModelIndex &source_parent) const override;
  bool filterAcceptsColumn(int source_column,
                           const QModelIndex &source_parent) const override;

  QVariant
  headerData(int section, Qt::Orientation orientation, int role) const override;
  QVariant data(const QModelIndex &index,
                int role = Qt::DisplayRole) const override;
  bool lessThan(const QModelIndex &left,
                const QModelIndex &right) const override;

  void hideNotValid(bool hide);
  void hideNotChecked(bool hide);
  void hideNotGrouped(bool hide);
  void setPeptideSearchString(QString peptide_search_string);
  void setSearchOn(QString search_on);
  void setPeptideListColumnDisplay(PeptideListColumn column, bool toggled);
  bool getPeptideListColumnDisplay(PeptideListColumn column) const;

  void resteItemDelegates() const;
  public slots:
  void onTableClicked(const QModelIndex &index);

  private:
  private:
  PeptideTableModel *_peptide_table_model_p;
  PeptideListWindow *_p_peptide_list_window;
  QString _peptide_search_string;
  bool _hide_not_valid   = true;
  bool _hide_not_checked = true;
  bool _hide_not_grouped = true;
  QString _search_on     = "peptide";
  std::vector<bool> _column_display;
  MassItemDelegate *_mass_delegate;
  MinuteItemDelegate *_minute_delegate;
  SecondItemDelegate *_second_delegate;
};
