
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "peptidetablemodel.h"

#include <QDebug>
#include <QColor>
#include <QSize>
#include "peptidelistwindow.h"
#include <pappsomspp/grouping/grppeptide.h>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/psm/deepprot/deepprotenum.h>
#include "../../core/labeling/label.h"
#include "../../utils/utils.h"
#include <pappsomspp/exception/exceptionnotfound.h>

PeptideTableModel::PeptideTableModel(PeptideListWindow *parent)
  : QAbstractTableModel(parent)
{
  _p_peptide_list_window = parent;
  // ui->tableView->show();
  // QModelIndex topLeft = createIndex(0,0);
  // emit a signal to make the view reread identified data
  // emit dataChanged(topLeft, topLeft);
}


void
PeptideTableModel::setProteinMatch(ProteinMatch *p_protein_match)
{
  qDebug() << "begin ";

  beginResetModel();
  _p_protein_match = p_protein_match;
  endResetModel();
  // QModelIndex topLeft = createIndex(0,0);
  // QModelIndex bottomRight = createIndex(rowCount(),columnCount());

  _engine_columns_to_display.clear();
  bool first = true;
  for(auto &&peptide_match : _p_protein_match->getPeptideMatchList())
    {
      if(first)
        {
          QVariant var = peptide_match.getPeptideEvidence()->getParam(
            PeptideEvidenceParam::peptide_prophet_probability);
          if(!var.isNull())
            {
              _engine_columns_to_display.insert(
                PeptideListColumn::peptide_prophet_probability);
            }
          var = peptide_match.getPeptideEvidence()->getParam(
            PeptideEvidenceParam::peptide_inter_prophet_probability);
          if(!var.isNull())
            {
              _engine_columns_to_display.insert(
                PeptideListColumn::peptide_inter_prophet_probability);
            }
          first = false;
        }
      IdentificationEngine engine =
        peptide_match.getPeptideEvidence()->getIdentificationEngine();
      if(engine == IdentificationEngine::XTandem)
        {
          _engine_columns_to_display.insert(
            PeptideListColumn::tandem_hyperscore);
        }
      else if(engine == IdentificationEngine::mascot)
        {
          _engine_columns_to_display.insert(PeptideListColumn::mascot_score);
          _engine_columns_to_display.insert(
            PeptideListColumn::mascot_expectation_value);
        }
      else if(engine == IdentificationEngine::OMSSA)
        {
          _engine_columns_to_display.insert(PeptideListColumn::omssa_evalue);
          _engine_columns_to_display.insert(PeptideListColumn::omssa_pvalue);
        }
      else if(engine == IdentificationEngine::MSGFplus)
        {
          _engine_columns_to_display.insert(PeptideListColumn::msgfplus_raw);
          _engine_columns_to_display.insert(PeptideListColumn::msgfplus_denovo);
          _engine_columns_to_display.insert(PeptideListColumn::msgfplus_energy);
          _engine_columns_to_display.insert(
            PeptideListColumn::msgfplus_SpecEValue);
          _engine_columns_to_display.insert(PeptideListColumn::msgfplus_EValue);
          _engine_columns_to_display.insert(
            PeptideListColumn::msgfplus_isotope_error);
        }
      else if(engine == IdentificationEngine::Comet)
        {
          _engine_columns_to_display.insert(PeptideListColumn::comet_deltacn);
          _engine_columns_to_display.insert(
            PeptideListColumn::comet_deltacnstar);
          _engine_columns_to_display.insert(
            PeptideListColumn::comet_expectation_value);
          _engine_columns_to_display.insert(PeptideListColumn::comet_sprank);
          _engine_columns_to_display.insert(PeptideListColumn::comet_spscore);
          _engine_columns_to_display.insert(PeptideListColumn::comet_xcorr);
        }
      else if(engine == IdentificationEngine::DeepProt)
        {
          _engine_columns_to_display.insert(
            PeptideListColumn::deepprot_original_count);
          _engine_columns_to_display.insert(
            PeptideListColumn::deepprot_fitted_count);
          _engine_columns_to_display.insert(
            PeptideListColumn::deepprot_mass_delta);
          _engine_columns_to_display.insert(
            PeptideListColumn::deepprot_delta_positions);
          _engine_columns_to_display.insert(
            PeptideListColumn::deepprot_match_type);
          _engine_columns_to_display.insert(PeptideListColumn::deepprot_status);
        }
    }

  qDebug() << _engine_columns_to_display.size();
  _p_peptide_list_window->resizeColumnsToContents();
  // emit dataChanged(topLeft, topLeft);
  // emit headerDataChanged(Qt::Horizontal, 0,33);
  emit layoutAboutToBeChanged();
  emit layoutChanged();
  qDebug() << "PeptideTableModel::setProteinMatch end ";
}

ProteinMatch *
PeptideTableModel::getProteinMatch()
{
  return _p_protein_match;
}
int
PeptideTableModel::rowCount(const QModelIndex &parent [[maybe_unused]]) const
{
  if(_p_protein_match != nullptr)
    {
      // qDebug() << "PeptideTableModel::rowCount(const QModelIndex &parent ) "
      // << _p_protein_match->getPeptideMatchList().size();
      return (int)_p_protein_match->getPeptideMatchList().size();
    }
  return 0;
}
int
PeptideTableModel::columnCount(const QModelIndex &parent [[maybe_unused]]) const
{
  return 50;
}
QVariant
PeptideTableModel::headerData(int section,
                              Qt::Orientation orientation,
                              int role) const
{
  if(orientation == Qt::Horizontal)
    {
      switch(role)
        {
          case Qt::DisplayRole:
            return QVariant(getTitle(section));
            break;
          case Qt::ToolTipRole:
            return QVariant(getDescription(section));
            break;
        }
    }
  // if
  // (_p_peptide_list_window->getProjectWindow()->getProjectP()->getLabelingMethodSp().get()
  // == nullptr) {
  return QVariant();
}
QVariant
PeptideTableModel::data(const QModelIndex &index, int role) const
{
  // generate a log message when this method gets called
  int row = index.row();
  int col = index.column();
  // qDebug() << QString("row %1, col%2, role %3")
  //         .arg(row).arg(col).arg(role);

  pappso::QualifiedMassSpectrum mass_spectrum;

  switch(role)
    {
      case Qt::CheckStateRole:

        if(col == 0) // add a checkbox to cell(1,0)
          {
            if(_p_protein_match->getPeptideMatchList()
                 .at(row)
                 .getPeptideEvidence()
                 ->isChecked())
              {
                return Qt::Checked;
              }
            else
              {
                return Qt::Unchecked;
              }
          }
        break;
      case Qt::BackgroundRole:
        if(_p_protein_match->getPeptideMatchList()
             .at(row)
             .getPeptideEvidence()
             ->isValid() == false)
          {
            return QVariant(QColor("grey"));
          }
        break;
      case Qt::SizeHintRole:
        // qDebug() << "ProteinTableModel::headerData " <<
        // ProteinTableModel::getColumnWidth(section);
        return QSize(PeptideTableModel::getColumnWidth(col), 30);
        break;
      case Qt::ToolTipRole:
        if(col == (std::int8_t)PeptideListColumn::sample)
          {
            return QVariant(_p_protein_match->getPeptideMatchList()
                              .at(row)
                              .getPeptideEvidence()
                              ->getIdentificationDataSource()
                              ->getMsRunSp()
                              ->getFileName());
          }
        break;
      case Qt::DisplayRole:
        if(_p_protein_match == nullptr)
          {
          }
        else
          {
            pappso::GrpPeptide *p_grp_peptide;
            const Label *p_label;
            GroupingGroup *p_grp;
            switch(col)
              {
                case(std::int8_t)PeptideListColumn::checked:
                  return QVariant();
                  break;

                case(std::int8_t)PeptideListColumn::peptide_grouping_id:
                  p_grp_peptide = _p_protein_match->getPeptideMatchList()
                                    .at(row)
                                    .getPeptideEvidence()
                                    ->getGrpPeptideSp()
                                    .get();
                  if(p_grp_peptide != nullptr)
                    return p_grp_peptide->getGroupingId();
                  return QVariant();
                  break;
                case(std::int8_t)PeptideListColumn::engine:
                  return Utils::getIdentificationEngineName(
                    _p_protein_match->getPeptideMatchList()
                      .at(row)
                      .getPeptideEvidence()
                      ->getIdentificationEngine());
                  break;
                case(std::int8_t)PeptideListColumn::sample:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getIdentificationDataSource()
                    ->getSampleName();
                  break;
                case(std::int8_t)PeptideListColumn::scan:
                  return QVariant(
                    (quint32)_p_protein_match->getPeptideMatchList()
                      .at(row)
                      .getPeptideEvidence()
                      ->getScanNumber());
                  break;
                case(std::int8_t)PeptideListColumn::spectrum_index:
                  try
                    {
                      return QVariant(
                        (quint32)_p_protein_match->getPeptideMatchList()
                          .at(row)
                          .getPeptideEvidence()
                          ->getSpectrumIndex());
                    }
                  catch(pappso::ExceptionNotFound &notfound)
                    {
                      // no spectrum index
                    }
                  break;
                case(std::int8_t)PeptideListColumn::rtmin:
                  return QVariant(
                    (qreal)(_p_protein_match->getPeptideMatchList()
                              .at(row)
                              .getPeptideEvidence()
                              ->getRetentionTime() /
                            60));
                  break;
                case(std::int8_t)PeptideListColumn::rt:
                  return QVariant((qreal)_p_protein_match->getPeptideMatchList()
                                    .at(row)
                                    .getPeptideEvidence()
                                    ->getRetentionTime());
                  break;
                case(std::int8_t)PeptideListColumn::charge:
                  return QVariant(
                    (quint32)_p_protein_match->getPeptideMatchList()
                      .at(row)
                      .getPeptideEvidence()
                      ->getCharge());
                  break;

                case(std::int8_t)PeptideListColumn::ion_mobility_begin:
                  try
                    {
                      mass_spectrum = _p_protein_match->getPeptideMatchList()
                                        .at(row)
                                        .getPeptideEvidence()
                                        ->getQualifiedMassSpectrum(false);

                      qDebug();
                      return QVariant(mass_spectrum.getParameterValue(
                        pappso::QualifiedMassSpectrumParameter::
                          OneOverK0begin));
                    }
                  catch(pappso::ExceptionNotFound &notfound)
                    {
                      // no spectrum index
                    }
                  break;

                case(std::int8_t)PeptideListColumn::ion_mobility_end:
                  try
                    {
                      mass_spectrum = _p_protein_match->getPeptideMatchList()
                                        .at(row)
                                        .getPeptideEvidence()
                                        ->getQualifiedMassSpectrum(false);

                      qDebug();
                      return QVariant(mass_spectrum.getParameterValue(
                        pappso::QualifiedMassSpectrumParameter::OneOverK0end));
                    }
                  catch(pappso::ExceptionNotFound &notfound)
                    {
                      // no spectrum index
                    }
                  break;

                case(std::int8_t)PeptideListColumn::collision_energy:
                  try
                    {
                      mass_spectrum = _p_protein_match->getPeptideMatchList()
                                        .at(row)
                                        .getPeptideEvidence()
                                        ->getQualifiedMassSpectrum(false);

                      qDebug();
                      return QVariant(mass_spectrum.getParameterValue(
                        pappso::QualifiedMassSpectrumParameter::
                          CollisionEnergy));
                    }
                  catch(pappso::ExceptionNotFound &notfound)
                    {
                      // no spectrum index
                    }
                  break;
                case(std::int8_t)PeptideListColumn::experimental_mz:
                  return QVariant(_p_protein_match->getPeptideMatchList()
                                    .at(row)
                                    .getPeptideEvidence()
                                    ->getExperimentalMz());
                  break;
                case(std::int8_t)PeptideListColumn::sequence_nter:

                  return _p_protein_match->getFlankingNterRegion(
                    _p_protein_match->getPeptideMatchList().at(row), 1);
                  break;
                case(std::int8_t)PeptideListColumn::sequence:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getPeptideXtpSp()
                    .get()
                    ->getSequence();
                  break;

                case(std::int8_t)PeptideListColumn::sequence_cter:

                  return _p_protein_match->getFlankingCterRegion(
                    _p_protein_match->getPeptideMatchList().at(row), 1);
                  break;
                case(std::int8_t)PeptideListColumn::modifs:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getPeptideXtpSp()
                    .get()
                    ->getModifString();
                  break;
                case(std::int8_t)PeptideListColumn::label:
                  p_label = _p_protein_match->getPeptideMatchList()
                              .at(row)
                              .getPeptideEvidence()
                              ->getPeptideXtpSp()
                              .get()
                              ->getLabel();
                  if(p_label != nullptr)
                    {
                      return QVariant(p_label->getXmlId());
                    }
                  else
                    {
                      return QVariant();
                    }
                  break;
                case(std::int8_t)PeptideListColumn::start:
                  return QVariant(
                    (quint32)_p_protein_match->getPeptideMatchList()
                      .at(row)
                      .getStart() +
                    1);
                  break;
                case(std::int8_t)PeptideListColumn::length:
                  return QVariant(
                    (quint32)_p_protein_match->getPeptideMatchList()
                      .at(row)
                      .getPeptideEvidence()
                      ->getPeptideXtpSp()
                      .get()
                      ->size());
                  break;
                case(std::int8_t)PeptideListColumn::used:
                  p_grp = _p_protein_match->getGroupingGroupSp().get();
                  if(p_grp != nullptr)
                    return QVariant((qreal)p_grp->countSubgroupPresence(
                      _p_protein_match->getPeptideMatchList()
                        .at(row)
                        .getPeptideEvidence()));
                  return QVariant();
                  break;
                case(std::int8_t)PeptideListColumn::subgroups:
                  p_grp = _p_protein_match->getGroupingGroupSp().get();
                  if(p_grp != nullptr)
                    return QVariant(p_grp
                                      ->getSubgroupIdList(
                                        _p_protein_match->getPeptideMatchList()
                                          .at(row)
                                          .getPeptideEvidence())
                                      .join(" "));
                  return QVariant();
                  break;
                case(std::int8_t)PeptideListColumn::Evalue:
                  return QVariant((qreal)_p_protein_match->getPeptideMatchList()
                                    .at(row)
                                    .getPeptideEvidence()
                                    ->getEvalue());
                  break;
                case(std::int8_t)PeptideListColumn::qvalue:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::pappso_qvalue);
                  break;
                case(std::int8_t)PeptideListColumn::experimental_mhplus:
                  return QVariant(_p_protein_match->getPeptideMatchList()
                                    .at(row)
                                    .getPeptideEvidence()
                                    ->getExperimentalMhplus());
                  break;
                case(std::int8_t)PeptideListColumn::theoretical_mhplus:
                  return QVariant(_p_protein_match->getPeptideMatchList()
                                    .at(row)
                                    .getPeptideEvidence()
                                    ->getPeptideXtpSp()
                                    .get()
                                    ->getMz(1));
                  break;
                case(std::int8_t)PeptideListColumn::delta_mhplus:
                  return QVariant((qreal)_p_protein_match->getPeptideMatchList()
                                    .at(row)
                                    .getPeptideEvidence()
                                    ->getDeltaMass());
                  break;
                case(std::int8_t)PeptideListColumn::delta_ppm:
                  return QVariant((qreal)_p_protein_match->getPeptideMatchList()
                                    .at(row)
                                    .getPeptideEvidence()
                                    ->getPpmDeltaMass());
                  break;
                case(std::int8_t)PeptideListColumn::tandem_hyperscore:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::tandem_hyperscore);
                  break;
                case(std::int8_t)PeptideListColumn::mascot_score:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::mascot_score);
                  break;
                case(std::int8_t)PeptideListColumn::mascot_expectation_value:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::mascot_expectation_value);
                  break;

                case(std::int8_t)PeptideListColumn::peptide_prophet_probability:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(
                      PeptideEvidenceParam::peptide_prophet_probability);
                  break;
                case(std::int8_t)
                  PeptideListColumn::peptide_inter_prophet_probability:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(
                      PeptideEvidenceParam::peptide_inter_prophet_probability);
                  break;
                case(std::int8_t)PeptideListColumn::omssa_evalue:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::omssa_evalue);
                  break;
                case(std::int8_t)PeptideListColumn::omssa_pvalue:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::omssa_pvalue);
                  break;
                case(std::int8_t)PeptideListColumn::msgfplus_denovo:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::msgfplus_denovo);
                  break;
                case(std::int8_t)PeptideListColumn::msgfplus_energy:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::msgfplus_energy);
                  break;
                case(std::int8_t)PeptideListColumn::msgfplus_EValue:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::msgfplus_EValue);
                  break;
                case(std::int8_t)PeptideListColumn::msgfplus_raw:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::msgfplus_raw);
                  break;
                case(std::int8_t)PeptideListColumn::msgfplus_SpecEValue:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::msgfplus_SpecEValue);
                  break;

                case(std::int8_t)PeptideListColumn::msgfplus_isotope_error:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::msgfplus_isotope_error);
                  break;
                case(std::int8_t)PeptideListColumn::comet_xcorr:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::comet_xcorr);
                  break;
                case(std::int8_t)PeptideListColumn::comet_deltacn:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::comet_deltacn);
                  break;
                case(std::int8_t)PeptideListColumn::comet_deltacnstar:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::comet_deltacnstar);
                  break;
                case(std::int8_t)PeptideListColumn::comet_spscore:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::comet_spscore);
                  break;
                case(std::int8_t)PeptideListColumn::comet_sprank:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::comet_sprank);
                  break;
                case(std::int8_t)PeptideListColumn::comet_expectation_value:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::comet_expectation_value);
                  break;
                case(std::int8_t)PeptideListColumn::deepprot_original_count:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::deepprot_original_count);
                  break;

                case(std::int8_t)PeptideListColumn::deepprot_fitted_count:
                  return _p_protein_match->getPeptideMatchList()
                    .at(row)
                    .getPeptideEvidence()
                    ->getParam(PeptideEvidenceParam::deepprot_fitted_count);
                  break;

                case(std::int8_t)PeptideListColumn::deepprot_match_type:
                  {
                    QVariant param(
                      _p_protein_match->getPeptideMatchList()
                        .at(row)
                        .getPeptideEvidence()
                        ->getParam(PeptideEvidenceParam::deepprot_match_type));
                    if(param.isNull())
                      return QString();
                    return pappso::DeepProtEnumStr::toString(
                      (pappso::DeepProtMatchType)param.toUInt());
                  }
                  break;


                case(std::int8_t)PeptideListColumn::deepprot_status:
                  {
                    QVariant param(
                      _p_protein_match->getPeptideMatchList()
                        .at(row)
                        .getPeptideEvidence()
                        ->getParam(PeptideEvidenceParam::
                                     deepprot_peptide_candidate_status));
                    if(param.isNull())
                      return QString();
                    return pappso::DeepProtEnumStr::toString(
                      (pappso::DeepProtPeptideCandidateStatus)param.toUInt());
                  }
                  break;
                case(std::int8_t)PeptideListColumn::deepprot_mass_delta:
                  {
                    QVariant param(
                      _p_protein_match->getPeptideMatchList()
                        .at(row)
                        .getPeptideEvidence()
                        ->getParam(PeptideEvidenceParam::deepprot_mass_delta));
                    if(param.isNull())
                      return QString();
                    return param.toDouble();
                  }
                  break;


                case(std::int8_t)PeptideListColumn::deepprot_delta_positions:
                  {
                    QVariant param(
                      _p_protein_match->getPeptideMatchList()
                        .at(row)
                        .getPeptideEvidence()
                        ->getParam(
                          PeptideEvidenceParam::deepprot_delta_positions));
                    if(param.isNull())
                      return QString();
                    return param;
                  }
                  break;
              }
          }
        return QString();
    }
  return QVariant();
}

void
PeptideTableModel::onPeptideDataChanged()
{
  qDebug() << "begin " << rowCount();
  emit layoutAboutToBeChanged();
  emit layoutChanged();
}


PeptideListColumn
PeptideTableModel::getPeptideListColumn(std::int8_t column)
{
  return static_cast<PeptideListColumn>(column);
}
const QString
PeptideTableModel::getTitle(PeptideListColumn column)
{
  qDebug() << " begin ";
  return PeptideTableModel::getTitle((std::int8_t)column);
  // qDebug() << "ProteinTableModel::getTitle end ";
}
const QString
PeptideTableModel::getDescription(PeptideListColumn column)
{
  // qDebug() << "PeptideTableModel::columnCount begin ";
  return PeptideTableModel::getDescription((std::int8_t)column);
  // qDebug() << "ProteinTableModel::columnCount end ";
}

const QString
PeptideTableModel::getTitle(std::int8_t column)
{

  // qDebug() << "PeptideTableModel::getTitle begin " << column;
  switch(column)
    {

      case(std::int8_t)PeptideListColumn::checked:
        return "Checked";
        break;
      case(std::int8_t)PeptideListColumn::peptide_grouping_id:
        return "Peptide ID";
        break;
      case(std::int8_t)PeptideListColumn::engine:
        return "Engine";
        break;
      case(std::int8_t)PeptideListColumn::sample:
        return "Sample";
        break;
      case(std::int8_t)PeptideListColumn::scan:
        return "Scan";
        break;
      case(std::int8_t)PeptideListColumn::spectrum_index:
        return "Index";
        break;
      case(std::int8_t)PeptideListColumn::rtmin:
        return "RT (min)";
        break;
      case(std::int8_t)PeptideListColumn::rt:
        return "RT (s)";
        break;
      case(std::int8_t)PeptideListColumn::ion_mobility_begin:
        return "1/K0 start";
        break;
      case(std::int8_t)PeptideListColumn::ion_mobility_end:
        return "1/K0 stop";
        break;
      case(std::int8_t)PeptideListColumn::collision_energy:
        return "Collision energy (eV)";
        break;
      case(std::int8_t)PeptideListColumn::charge:
        return "Charge";
        break;
      case(std::int8_t)PeptideListColumn::experimental_mz:
        return "Obs. m/z";
        break;
      case(std::int8_t)PeptideListColumn::sequence_nter:
        return "N-term";
        break;
      case(std::int8_t)PeptideListColumn::sequence:
        return "Sequence";
        break;
      case(std::int8_t)PeptideListColumn::sequence_cter:
        return "C-term";
        break;
      case(std::int8_t)PeptideListColumn::modifs:
        return "Modifs";
        break;
      case(std::int8_t)PeptideListColumn::label:
        return "Label";
        break;
      case(std::int8_t)PeptideListColumn::start:
        return "Start";
        break;
      case(std::int8_t)PeptideListColumn::length:
        return "Length";
        break;
      case(std::int8_t)PeptideListColumn::used:
        return "Used";
        break;
      case(std::int8_t)PeptideListColumn::subgroups:
        return "Subgroups";
        break;
      case(std::int8_t)PeptideListColumn::Evalue:
        return "E-value";
        break;
      case(std::int8_t)PeptideListColumn::qvalue:
        return "Cumulated FDR";
        break;
      case(std::int8_t)PeptideListColumn::experimental_mhplus:
        return "Obs. [M+H]+";
        break;
      case(std::int8_t)PeptideListColumn::theoretical_mhplus:
        return "Theor. [M+H]+";
        break;
      case(std::int8_t)PeptideListColumn::delta_mhplus:
        return "Delta [M+H]+";
        break;
      case(std::int8_t)PeptideListColumn::delta_ppm:
        return "Delta (ppm)";
        break;
      case(std::int8_t)PeptideListColumn::tandem_hyperscore:
        return "HyperScore";
        break;
      case(std::int8_t)PeptideListColumn::mascot_score:
        return "Mascot score";
        break;
      case(std::int8_t)PeptideListColumn::mascot_expectation_value:
        return "Mascot E-value";
        break;
      case(std::int8_t)PeptideListColumn::peptide_prophet_probability:
        return "Prophet prob.";
        break;
      case(std::int8_t)PeptideListColumn::peptide_inter_prophet_probability:
        return "Inter prophet prob.";
        break;
      case(std::int8_t)PeptideListColumn::omssa_evalue:
        return "OMSSA E-value";
        break;
      case(std::int8_t)PeptideListColumn::omssa_pvalue:
        return "OMSSA p-value";
        break;
      case(std::int8_t)PeptideListColumn::msgfplus_denovo:
        return "MS-GF de novo";
        break;
      case(std::int8_t)PeptideListColumn::msgfplus_energy:
        return "MS-GF energy";
        break;
      case(std::int8_t)PeptideListColumn::msgfplus_EValue:
        return "MS-GF E-value";
        break;
      case(std::int8_t)PeptideListColumn::msgfplus_raw:
        return "MS-GF raw score";
        break;
      case(std::int8_t)PeptideListColumn::msgfplus_SpecEValue:
        return "MS-GF spectral E-value";
        break;
      case(std::int8_t)PeptideListColumn::msgfplus_isotope_error:
        return "MS-GF isotope error";
        break;

      case(std::int8_t)PeptideListColumn::comet_xcorr:
        return "Comet XCorr";
        break;

      case(std::int8_t)PeptideListColumn::comet_deltacn:
        return "Comet DeltaCn";
        break;
      case(std::int8_t)PeptideListColumn::comet_deltacnstar:
        return "Comet DeltaCnStar";
        break;
      case(std::int8_t)PeptideListColumn::comet_spscore:
        return "Comet SpScore";
        break;
      case(std::int8_t)PeptideListColumn::comet_sprank:
        return "Comet SpRank";
        break;
      case(std::int8_t)PeptideListColumn::comet_expectation_value:
        return "Comet E-value";
        break;
      case(std::int8_t)PeptideListColumn::deepprot_original_count:
        return "DeepProt matched peaks";
        break;
      case(std::int8_t)PeptideListColumn::deepprot_fitted_count:
        return "DeepProt fitted peaks";
        break;

      case(std::int8_t)PeptideListColumn::deepprot_match_type:
        return "DeepProt match type";
        break;

      case(std::int8_t)PeptideListColumn::deepprot_status:
        return "DeepProt status";
        break;
      case(std::int8_t)PeptideListColumn::deepprot_mass_delta:
        return "DeepProt mass delta";
        break;
      case(std::int8_t)PeptideListColumn::deepprot_delta_positions:
        return "DeepProt mass delta pos.";
        break;
    }
  return "";
}

const QString
PeptideTableModel::getDescription(std::int8_t column)
{

  qDebug() << "ProteinTableModel::getDescription begin " << column;
  switch(column)
    {

      case(std::int8_t)PeptideListColumn::checked:
        return "Manual protein check";
        break;

      case(std::int8_t)PeptideListColumn::peptide_grouping_id:
        return "Unique peptide identifier within this grouping experiment";
        break;
      case(std::int8_t)PeptideListColumn::engine:
        return "Search engine";
        break;
      case(std::int8_t)PeptideListColumn::sample:
        return "MS sample name";
        break;
      case(std::int8_t)PeptideListColumn::scan:
        return "Scan number";
        break;
      case(std::int8_t)PeptideListColumn::spectrum_index:
        return "Spectrum index";
        break;
      case(std::int8_t)PeptideListColumn::rtmin:
        return "Retention time in minutes";
        break;
      case(std::int8_t)PeptideListColumn::rt:
        return "Retention time in seconds";
        break;
      case(std::int8_t)PeptideListColumn::ion_mobility_begin:
        return "Ion mobility start (if available)";
        break;
      case(std::int8_t)PeptideListColumn::ion_mobility_end:
        return "Ion mobility stop (if available)";
        break;
      case(std::int8_t)PeptideListColumn::collision_energy:
        return "Collision Energy (if available)";
        break;
      case(std::int8_t)PeptideListColumn::charge:
        return "Peptide ion charge";
        break;
      case(std::int8_t)PeptideListColumn::experimental_mz:
        return "Observed m/z (experimental m/z of precursor)";
        break;
      case(std::int8_t)PeptideListColumn::sequence_nter:
        return "Flanking protein amino acid on peptide N-term side";
        break;
      case(std::int8_t)PeptideListColumn::sequence:
        return "Peptide sequence";
        break;
      case(std::int8_t)PeptideListColumn::sequence_cter:
        return "Flanking protein amino acid on peptide C-term side";
        break;
      case(std::int8_t)PeptideListColumn::modifs:
        return "Peptide modifications";
        break;
      case(std::int8_t)PeptideListColumn::label:
        return "Peptide label";
        break;
      case(std::int8_t)PeptideListColumn::start:
        return "Peptide start position on protein";
        break;
      case(std::int8_t)PeptideListColumn::length:
        return "Peptide length";
        break;
      case(std::int8_t)PeptideListColumn::used:
        return "Number of subgroups in which this peptide is used";
        break;
      case(std::int8_t)PeptideListColumn::subgroups:
        return "List of subgroups in which this peptide is used";
        break;
      case(std::int8_t)PeptideListColumn::Evalue:
        return "Peptide Evalue";
        break;
      case(std::int8_t)PeptideListColumn::qvalue:
        return "Peptide q-value";
        break;
      case(std::int8_t)PeptideListColumn::experimental_mhplus:
        return "Peptide ion observed mass [M+H]+";
        break;
      case(std::int8_t)PeptideListColumn::theoretical_mhplus:
        return "Peptide ion theoretical mass [M+H]+";
        break;
      case(std::int8_t)PeptideListColumn::delta_mhplus:
        return "Mass difference in Dalton between the peptide's observed and "
               "theoretical masses (observed - theoretical)";
        break;
      case(std::int8_t)PeptideListColumn::delta_ppm:
        return "Mass difference in ppm between the peptide's observed and "
               "theoretical masses (observed - theoretical)";
        break;
      case(std::int8_t)PeptideListColumn::tandem_hyperscore:
        return "X!Tandem HyperScore";
        break;
      case(std::int8_t)PeptideListColumn::mascot_score:
        return "Mascot ion score";
        break;
      case(std::int8_t)PeptideListColumn::mascot_expectation_value:
        return "Mascot expectation value";
        break;
      case(std::int8_t)PeptideListColumn::peptide_prophet_probability:
        return "Peptide prophet probability";
        break;
      case(std::int8_t)PeptideListColumn::peptide_inter_prophet_probability:
        return "Peptide inter prophet probability";
        break;
      case(std::int8_t)PeptideListColumn::omssa_evalue:
        return "OMSSA E-value";
        break;
      case(std::int8_t)PeptideListColumn::omssa_pvalue:
        return "OMSSA p-value";
        break;
      case(std::int8_t)PeptideListColumn::msgfplus_denovo:
        return "MS-GF de novo";
        break;
      case(std::int8_t)PeptideListColumn::msgfplus_energy:
        return "MS-GF energy";
        break;
      case(std::int8_t)PeptideListColumn::msgfplus_EValue:
        return "MS-GF E-value";
        break;
      case(std::int8_t)PeptideListColumn::msgfplus_raw:
        return "MS-GF raw score";
        break;
      case(std::int8_t)PeptideListColumn::msgfplus_SpecEValue:
        return "MS-GF spectral E-value";
        break;
      case(std::int8_t)PeptideListColumn::msgfplus_isotope_error:
        return "MS-GF isotope error";
        break;
      case(std::int8_t)PeptideListColumn::comet_xcorr:
        return "Comet result 'XCorr'";
        break;
      case(std::int8_t)PeptideListColumn::comet_deltacn:
        return "Comet result 'DeltaCn'";
        break;
      case(std::int8_t)PeptideListColumn::comet_deltacnstar:
        return "Comet result 'DeltaCnStar'";
        break;
      case(std::int8_t)PeptideListColumn::comet_spscore:
        return "Comet result 'SpScore'";
        break;
      case(std::int8_t)PeptideListColumn::comet_sprank:
        return "Comet result 'SpRank'";
        break;
      case(std::int8_t)PeptideListColumn::comet_expectation_value:
        return "Comet result 'Expectation value'";
        break;
      case(std::int8_t)PeptideListColumn::deepprot_original_count:
        return "DeepProt number of matched peaks before specfit";
        break;
      case(std::int8_t)PeptideListColumn::deepprot_fitted_count:
        return "DeepProt number of matched peaks after specfit";
        break;

      case(std::int8_t)PeptideListColumn::deepprot_match_type:
        return "DeepProt spectrum match type";
        break;
      case(std::int8_t)PeptideListColumn::deepprot_status:
        return "DeepProt peptide candidate status";
        break;
      case(std::int8_t)PeptideListColumn::deepprot_mass_delta:
        return "DeepProt PSM mass delta";
        break;
      case(std::int8_t)PeptideListColumn::deepprot_delta_positions:
        return "DeepProt PSM mass delta positions";
        break;
    }
  throw pappso::PappsoException(
    QObject::tr("no title fot column number %1").arg(column));
  return "";
}

int
PeptideTableModel::getColumnWidth(int column)
{
  // qDebug() << "PeptideTableModel::getColumnWidth " << column;
  switch(column)
    {

      case(int)PeptideListColumn::checked:
        break;

      case(int)PeptideListColumn::peptide_grouping_id:
        return 120;
        break;
      case(int)PeptideListColumn::sample:
        return 250;
        break;
      case(int)PeptideListColumn::sequence:
        return 250;
        break;
    }
  return 100;
}


bool
PeptideTableModel::hasColumn(PeptideListColumn column)
{
  if((std::int8_t)column < m_nonIdentificationEngineColumnLimit)
    {
      return true;
    }
  qDebug() << _engine_columns_to_display.size();
  if(_engine_columns_to_display.find(column) !=
     _engine_columns_to_display.end())
    {
      return true;
    }
  return false;
}
