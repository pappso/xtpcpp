/**
 * \file /gui/widgets/massitemdelegate.h
 * \date 23/3/2018
 * \author Olivier Langella
 * \brief writes mass in table columns with convenient precision
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/
#pragma once

#include <QStyledItemDelegate>

class MinuteItemDelegate : public QStyledItemDelegate
{

  Q_OBJECT

  public:
  MinuteItemDelegate(QObject *parent = 0) : QStyledItemDelegate(parent)
  {
  }

  QString
  displayText(const QVariant &value, const QLocale &locale) const override
  {
    Q_UNUSED(locale);
    QString str = QString("%1'").arg(QString::number(value.toDouble(), 'f', 2));
    return str;
  }
};

class SecondItemDelegate : public QStyledItemDelegate
{

  Q_OBJECT

  public:
  SecondItemDelegate(QObject *parent = 0) : QStyledItemDelegate(parent)
  {
  }

  QString
  displayText(const QVariant &value, const QLocale &locale) const override
  {
    Q_UNUSED(locale);

    QString str =
      QString("%1\"").arg(QString::number(value.toDouble(), 'f', 2));
    return str;
  }
};

class PercentItemDelegate : public QStyledItemDelegate
{

  Q_OBJECT

  public:
  PercentItemDelegate(QObject *parent = 0) : QStyledItemDelegate(parent)
  {
  }

  QString
  displayText(const QVariant &value, const QLocale &locale) const override
  {
    Q_UNUSED(locale);

    QString str =
      QString("%1 %").arg(QString::number(value.toDouble() * 100.0, 'f', 2));
    return str;
  }
};

class MassItemDelegate : public QStyledItemDelegate
{

  Q_OBJECT

  public:
  MassItemDelegate(QObject *parent = 0) : QStyledItemDelegate(parent)
  {
  }

  QString
  displayText(const QVariant &value, const QLocale &locale) const override
  {
    Q_UNUSED(locale);

    QString str = QString::number(value.toDouble(), 'f', 5);
    return str;
  }
};
