/**
 * \file gui/widgets/decoy_widget/decoywidget.h
 * \date 20/2/2018
 * \author Olivier Langella
 * \brief graphic widget to choose decoy files or regular expression
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#pragma once

#include <QWidget>
#include <QStandardItemModel>
#include "../../../files/fastafile.h"
#include "../../../core/project.h"


namespace Ui
{
class DecoyWidget;
}

class DecoyWidget : public QWidget
{
  Q_OBJECT

  public:
  explicit DecoyWidget(QWidget *parent);
  ~DecoyWidget();


  void setRegexpDecoyPattern(const QString &pattern);

  void setFastaFileList(std::vector<FastaFileSp> fasta_file_list);

  void setProjectDecoys(Project *p_project);
  void getProjectDecoys(const Project *p_project);

  public slots:

  signals:
  void changed();

  private slots:
  void doSelectDecoySource();
  void doSelectFastaFile();
  void doClearList();
  void doChanged();


  private:
  Ui::DecoyWidget *ui;

  bool _no_project = true;

  QStandardItemModel *_p_fasta_str_li;

  bool _emit_changed = true;
};
