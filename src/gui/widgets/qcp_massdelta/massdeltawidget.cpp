/**
 * \file src/gui/widgets/qcp_massdelta/massdeltawidget.cpp
 * \date 19/03/2021
 * \author Olivier Langella
 * \brief custom widget to display peptide evidence mass delta graph
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "massdeltawidget.h"
#include "ui_qcpmassdelta.h"
#include <QDebug>
#include <QVector>

MassDeltaWidget::MassDeltaWidget(QWidget *parent)
  : QWidget(parent), ui(new Ui::QcpMassDelta)
{
  qDebug();
  ui->setupUi(this);
  ui->qcpMassDelta->setMassDeltaWidget(this);

  setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

  ui->qcpMassDelta->setSizePolicy(QSizePolicy::Preferred,
                                  QSizePolicy::Expanding);

  qDebug();
}

MassDeltaWidget::~MassDeltaWidget()
{
  qDebug();
  delete ui;
  qDebug();
}


void
MassDeltaWidget::clearPeptideEvidenceList()
{
  ui->qcpMassDelta->reset();
  update();
}

void
MassDeltaWidget::setCurrentPeptideEvidenceSp(
  std::shared_ptr<PeptideEvidence> &currentPeptideEvidence)
{
  qDebug();
  emit peptideEvidenceChanged(currentPeptideEvidence);
}


void
MassDeltaWidget::setSelectedPeptideEvidenceSp(
  std::shared_ptr<PeptideEvidence> &currentPeptideEvidence)
{
  qDebug();
  emit selectedPeptideEvidenceChanged(currentPeptideEvidence);
}


void
MassDeltaWidget::addPeptideEvidenceList(
  const Project *p_project,
  const std::vector<std::shared_ptr<PeptideEvidence>> &peptide_evidence_list)
{
  qDebug();
  ui->qcpMassDelta->addGraph();
  ui->qcpMassDelta->graph(0)->setPen(QPen(QColor(255, 100, 0)));
  // ui->QcpGraphMassDelta->graph(0)->setBrush(
  // QBrush(QPixmap("./balboa.jpg"))); // fill with texture of specified image
  ui->qcpMassDelta->graph(0)->setLineStyle(QCPGraph::lsNone);
  ui->qcpMassDelta->graph(0)->setScatterStyle(
    QCPScatterStyle(QCPScatterStyle::ssDisc, 5));
  ui->qcpMassDelta->graph(0)->setName("decoy peptide evidence mass delta");

  ui->qcpMassDelta->addGraph();
  ui->qcpMassDelta->graph(1)->setPen(QPen(QColor(0, 100, 255)));
  // ui->QcpGraphMassDelta->graph(0)->setBrush(
  // QBrush(QPixmap("./balboa.jpg"))); // fill with texture of specified image
  ui->qcpMassDelta->graph(1)->setLineStyle(QCPGraph::lsNone);
  ui->qcpMassDelta->graph(1)->setScatterStyle(
    QCPScatterStyle(QCPScatterStyle::ssDisc, 5));
  ui->qcpMassDelta->graph(1)->setName("target peptide evidence mass delta");


  ui->qcpMassDelta->yAxis->grid()->setSubGridVisible(true);

  // interactions
  ui->qcpMassDelta->setInteraction(QCP::iRangeDrag, true);
  ui->qcpMassDelta->setInteraction(QCP::iRangeZoom, true);

  qDebug();
  ui->qcpMassDelta->setPeptideEvidenceList(p_project, peptide_evidence_list);
  qDebug();
}

void
MassDeltaWidget::mousePressEvent(QMouseEvent *event)
{
  qDebug() << "click";

  QWidget::mousePressEvent(event);
}
