/**
 * \file gui/widget/xic_extraction_method_widget/xicextractionmethodwidget.h
 * \date 17/02/2019
 * \author Olivier Langella
 * \brief choose Xic extraction method (sum or max)
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QWidget>
#include <QComboBox>
#include <pappsomspp/types.h>

class XicExtractionMethodWidget : public QWidget
{
  Q_OBJECT

  private:
  QComboBox *mp_methodComboBox;
  pappso::XicExtractMethod m_oldMethod = pappso::XicExtractMethod::max;

  private:
  Q_SLOT void setCurrentIndex(int);

  public:
  XicExtractionMethodWidget(QWidget *parent = 0);
  ~XicExtractionMethodWidget();
  pappso::XicExtractMethod getXicExtractionMethod() const;

  signals:
  void xicExtractionMethodChanged(pappso::XicExtractMethod method) const;
};
