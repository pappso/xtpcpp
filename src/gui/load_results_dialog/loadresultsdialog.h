
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef LOADRESULTSDIALOG_H
#define LOADRESULTSDIALOG_H

#include <QDialog>
#include <QStringListModel>
#include "../widgets/automatic_filter_widget/automaticfilterwidget.h"
#include "../../core/project.h"


namespace Ui
{
class LoadResultsDialog;
}


class LoadResultsDialog : public QDialog
{
  Q_OBJECT

  public:
  explicit LoadResultsDialog(QWidget *parent);
  ~LoadResultsDialog();

  AutomaticFilterParameters getAutomaticFilterParameters() const;
  QStringList getFileList() const;
  bool isIndividual() const;
  void setProjectContaminants(Project *p_project) const;
  public slots:
  void chooseFiles();
  void clearFileList();

  signals:

  private:
  Ui::LoadResultsDialog *ui;
  QStringListModel *_p_file_list;
};

#endif // LOADRESULTSDIALOG_H
