/**
 * \file gui/mcqr_run_view/mcqrparamwidget.cpp
 * \date 30/06/2021
 * \author Thomas Renne
 * \brief template of the Mcqr parameters for each script
 */

/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#include "mcqrparamwidget.h"
#include "mcqrrunview.h"
#include <QFileInfo>
#include <QDir>


McqrParamWidget::McqrParamWidget(McqrRunView *run_view,
                                 const McqrExperimentSp &p_mcqr_exp)
{
  qDebug() << "Other constructor";
  mp_mcqrRunWindow         = run_view;
  msp_mcqrExperimentLoaded = p_mcqr_exp;
  // m_rdataPath      = p_mcqr_exp.get()->getRdataFilePath();
  QDir rdata_dir =
    QFileInfo(msp_mcqrExperimentLoaded.get()->getRdataFilePath()).absoluteDir();
  mp_tmpPath = new QTemporaryDir(rdata_dir.absolutePath() + "/output");
  mp_tmpPath->setAutoRemove(true);
}


McqrParamWidget::~McqrParamWidget()
{
  qDebug() << "Close Mcqr Param Widget";
  mp_tmpPath->remove();
}

void
McqrParamWidget::changeStepAfterEndTag(QString end_name [[maybe_unused]])
{
}

void
McqrParamWidget::writeMcqrResultInRightFile(QString mcqr_result
                                            [[maybe_unused]])
{
}


const McqrExperimentSp &
McqrParamWidget::getMcqrExperimentSpLoaded() const
{
  return msp_mcqrExperimentLoaded;
}

McqrRunView *
McqrParamWidget::getMcqrRunParentView()
{
  return mp_mcqrRunWindow;
}

QString
McqrParamWidget::getOutputTmpPath()
{
  return QString(mp_tmpPath->path()).append(QDir::separator());
}

QString
McqrParamWidget::transformQStringListInRListFormat(QStringList list)
{
  QString concat_string = "\"" + list.join("\", \"") + "\"";
  return concat_string;
}

void
McqrParamWidget::saveModifiedRData(QString new_rdata_path)
{
  QString save_script;
  save_script = " save.image(file=\"" + new_rdata_path + "\")";
  mp_mcqrRunWindow->getRProcessRunning()->executeOffTheRecord(save_script);
}
