
/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@u-psud.fr>. - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QMainWindow>
#include <QAction>
#include <QMenu>
#include "msidentificationtablemodel.h"
#include "msidentificationtableproxymodel.h"
#include "ui_ms_identification_run_view.h"
#include "../../widgets/Alignment_group_menu/alignmentgroupmenu.h"
#include "../../masschroq_run_dialog/masschroqrundialog.h"

class ProjectWindow;

namespace Ui
{
class MsIdentificationView;
}

class MsIdentificationListWindow;
class MsIdentificationListQActionColumn : public QAction
{
  Q_OBJECT
  public:
  explicit MsIdentificationListQActionColumn(MsIdentificationListWindow *parent,
                                             msIdentificationListColumn column);
  ~MsIdentificationListQActionColumn();


  public slots:
  void doToggled(bool toggled);

  private:
  MsIdentificationListWindow *m_msid_list_window;
  msIdentificationListColumn m_column;
};


class MsIdentificationListWindow : public QMainWindow
{
  friend class MsIdentificationTableModel;
  friend class MsIdentificationTableProxyModel;

  Q_OBJECT

  public:
  explicit MsIdentificationListWindow(ProjectWindow *parent = 0);
  ~MsIdentificationListWindow();
  void setIdentificationDataSourceSpList(
    std::vector<IdentificationDataSourceSp> &identificationDataSourceSpList);
  std::vector<IdentificationDataSourceSp> getIdentificationDataSourceSpList();
  void setMsIdentificationListColumnDisplay(msIdentificationListColumn column,
                                            bool toggled);
  bool
  getMsIdentificationListColumnDisplay(msIdentificationListColumn column) const;
  void setAlignmentGroup(MsRunAlignmentGroupSp msrun_alignment_group);
  void resizeColumnsToContents();
  QModelIndexList getSelectedIndexes();
  MsIdentificationTableProxyModel *getMsIdentificationProxyModel();
  MsIdentificationTableModel *getMsIdentificationModel();
  void virtual closeEvent(QCloseEvent *event) override;

  public slots:
  void doExportAsOdsFile();
  void doProjectNameChanged(QString name);
  void doCopyPath(QAction *action);

  signals:
  void MsIdentificationDataChanged();
  void operateMassChroqExportDialog();

  protected slots:
  void doMassChroQParameters();
  void doSearchOn(QString search_on);
  void updateStatusBar();
  void onMsIdentificationSearchEdit(QString protein_search_string);
  void showGroupsContextMenu(const QPoint &);


  protected:
  void askEngineDetailView(IdentificationDataSourceSp *identificationEngine);

  private:
  Ui::MsIdentificationView *ui;
  MsIdentificationTableModel *mp_msidTableModel  = nullptr;
  MsIdentificationTableProxyModel *mp_proxyModel = nullptr;
  MassChroqRunDialog *mp_export_masschroq_dialog = nullptr;
  ProjectWindow *mp_projectWindow;
  AlignmentGroupsQMenu *mp_groups_context_menu = nullptr;
  QMenu *mp_copyPathMenu                       = nullptr;
  QModelIndex m_askIndex;
};
