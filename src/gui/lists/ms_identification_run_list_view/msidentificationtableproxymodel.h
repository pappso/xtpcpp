
/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@u-psud.fr>. - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QAbstractTableModel>
#include <QSortFilterProxyModel>
#include "../../../core/project.h"
#include "msidentificationtablemodel.h"
#include "../../widgets/massitemdelegate.h"

class MsIdentificationListWindow;

class MsIdentificationTableModel;

class MsIdentificationTableProxyModel : public QSortFilterProxyModel
{
  Q_OBJECT
  public:
  MsIdentificationTableProxyModel(
    MsIdentificationListWindow *m_msid_list_window,
    MsIdentificationTableModel *m_msid_table_model_p);
  virtual ~MsIdentificationTableProxyModel();
  bool filterAcceptsRow(int source_row,
                        const QModelIndex &source_parent) const override;
  bool filterAcceptsColumn(int source_column,
                           const QModelIndex &source_parent) const override;
  QVariant
  headerData(int section, Qt::Orientation orientation, int role) const override;
  QVariant data(const QModelIndex &index,
                int role = Qt::DisplayRole) const override;
  bool setData(const QModelIndex &index,
               const QVariant &new_data,
               int role = Qt::EditRole) override;
  Qt::ItemFlags flags(const QModelIndex &idx) const override;
  bool lessThan(const QModelIndex &left,
                const QModelIndex &right) const override;
  void setSearchOn(QString search_on);
  void setMsIdentificationSearchString(QString ms_id_search_string);
  void setMsIdentificationListColumnDisplay(msIdentificationListColumn column,
                                            bool toggled);
  bool
  getMsIdentificationListColumnDisplay(msIdentificationListColumn column) const;
  void resteItemDelegates() const;

  public slots:
  void onTableClicked(const QModelIndex &index);

  private:
  IdentificationDataSourceSp m_identificationEngine;
  MsIdentificationTableModel *m_msid_table_model_p;
  MsIdentificationListWindow *m_msid_list_window;
  QString m_ms_id_search_string;
  QString m_search_on = "ms identification";
  std::vector<bool> m_column_display;
  PercentItemDelegate *m_percent_delegate;
};
