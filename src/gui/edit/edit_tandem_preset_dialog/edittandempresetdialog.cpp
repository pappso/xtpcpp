/**
 * \file gui/edit_tandem_preset_dialog/edittandempresetdialog.cpp
 * \date 30/9/2017
 * \author Olivier Langella
 * \brief edit tandem preset dialog
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "edittandempresetdialog.h"
#include <QDebug>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/exception/exceptionnotfound.h>

#include "ui_edit_tandem_preset_dialog.h"
#include <QSettings>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>


EditTandemPresetDialog::EditTandemPresetDialog(QWidget *parent)
  : QDialog(parent), ui(new Ui::EditTandemPresetView)
{
  qDebug() << "EditTandemPresetDialog::EditTandemPresetDialog begin";
  ui->setupUi(this);
  this->setModal(true);

  ui->splitter->setStretchFactor(0, 1);
#if QT_VERSION >= 0x050000
  // Qt5 code
#else
  // Qt4 code

#endif
  qDebug() << "EditTandemPresetDialog::EditTandemPresetDialog end";
}

EditTandemPresetDialog::~EditTandemPresetDialog()
{
  delete ui;
  if(_p_tandem_preset_file != nullptr)
    {
      delete _p_tandem_preset_file;
    }
}

void
EditTandemPresetDialog::setTandemParametersFile(
  const TandemParametersFile &tandem_preset_file)
{

  try
    {
      if(tandem_preset_file.exists())
        {
          _p_tandem_preset_file = new TandemParametersFile(tandem_preset_file);
          _preset_directory =
            _p_tandem_preset_file->getAbsoluteDir().absolutePath();
          _tandem_params = _p_tandem_preset_file->getTandemParameters();
        }
      else
        {
          _p_tandem_preset_file = new TandemParametersFile(
            ":/tandem/resources/model/QExactive_analysis_FDR_nosemi.xml");
          _tandem_params = _p_tandem_preset_file->getTandemParameters();
          QSettings settings;
          _preset_directory =
            settings.value("path/tandemrun_preset_directory", "").toString();
          _p_tandem_preset_file->setDirectory(QDir(_preset_directory));
        }
    }

  catch(pappso::PappsoException &error)
    {
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
      QMessageBox::warning(
        this, tr("Error"), tr("Error :\n %1").arg(error.qwhat()));
    }

  fillPresetDirectoryLabel();
  fillPresetComboBox();
  populate();
}

void
EditTandemPresetDialog::newTandemParameters(
  const TandemParameters &tandem_params)
{
  _tandem_params = tandem_params;

  QSettings settings;
  _preset_directory =
    settings.value("path/tandemrun_preset_directory", "").toString();

  _p_tandem_preset_file =
    new TandemParametersFile(QString("%1/temp.xml").arg(_preset_directory));
  fillPresetDirectoryLabel();
  fillPresetComboBox();
  populate();
}


void
EditTandemPresetDialog::doCopy()
{
  QString preset_name;
  if(_p_tandem_preset_file == nullptr)
    {
      preset_name = "untitled";
    }
  else
    {
      preset_name = _p_tandem_preset_file->getMethodName() + "_copy";
    }
  bool ok;
  QString new_preset_file_name =
    QInputDialog::getText(this,
                          tr("Preset file name"),
                          tr("Choose the new preset file name:"),
                          QLineEdit::Normal,
                          preset_name,
                          &ok);
  if(ok && !new_preset_file_name.isEmpty())
    {
      ui->method_name_label->setText(tr("%1 loaded").arg(new_preset_file_name));
    }
  else
    {
      return;
    }

  readUi();
  if(_p_tandem_preset_file == nullptr)
    {

      throw pappso::PappsoException(
        QObject::tr("_p_tandem_preset_file == nullptr"));
    }
  else
    {
      delete _p_tandem_preset_file;
    }
  qDebug() << new_preset_file_name;
  _p_tandem_preset_file = new TandemParametersFile(
    QString("%1/%2.xml").arg(_preset_directory).arg(new_preset_file_name));
  doSave();
}


void
EditTandemPresetDialog::doSelectDir()
{
  try
    {

      QString directory = QFileDialog::getExistingDirectory(
        this, tr("Choose preset directory"), _preset_directory);

      QFileInfo my_dir(directory);
      if(!directory.isEmpty() && !directory.isNull())
        {
          if(!my_dir.isWritable())
            {
              throw pappso::PappsoException(
                QObject::tr(
                  "this directory is not writable, check permissions (%1)")
                  .arg(my_dir.absoluteFilePath()));
            }
          _preset_directory = directory;
          fillPresetDirectoryLabel();
          fillPresetComboBox();
        }
    }
  catch(pappso::PappsoException &error)
    {
      QMessageBox::warning(
        this,
        tr("Error :"),
        tr("Error choosing preset directory : %1").arg(error.qwhat()));
    }
}

void
EditTandemPresetDialog::doSave()
{
  try
    {
      readUi();
      if(_p_tandem_preset_file == nullptr)
        {
          throw pappso::PappsoException(
            QObject::tr("_p_tandem_preset_file == nullptr"));
        }
      _p_tandem_preset_file->setDirectory(QDir(_preset_directory));
      _p_tandem_preset_file->setTandemParameters(_tandem_params);
      QSettings settings;
      settings.setValue("path/tandemrun_preset_directory", _preset_directory);
      fillPresetComboBox();
      populate();
    }
  catch(pappso::PappsoException &error)
    {
      QMessageBox::warning(
        this,
        tr("Error :"),
        tr("Error saving parameter file : %1\n").arg(error.qwhat()));
    }
}

void
EditTandemPresetDialog::doLoad()
{
  int index = ui->preset_combo_box->currentIndex();
  if(index != -1)
    { // -1 for not found
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
      TandemParametersFile *p_tandem_preset_file = new TandemParametersFile(
        ui->preset_combo_box->itemData(index).value<QString>());
      if(p_tandem_preset_file->isTandemPresetFile())
        {
          if(_p_tandem_preset_file == nullptr)
            {
              throw pappso::PappsoException(
                QObject::tr("_p_tandem_preset_file == nullptr"));
            }
          else
            {
              delete _p_tandem_preset_file;
            }

          _p_tandem_preset_file = p_tandem_preset_file;
          try
            {
              qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
              _tandem_params = _p_tandem_preset_file->getTandemParameters();
              qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
              populate();
            }
          catch(pappso::PappsoException &error)
            {
              qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
              QMessageBox::warning(
                this,
                tr("Error :"),
                tr("Error reading %1 X!Tandem parameter file:\n %2")
                  .arg(p_tandem_preset_file->getAbsoluteFilePath())
                  .arg(error.qwhat()));
            }
        }
      else
        {
          QMessageBox::warning(
            this,
            tr("Error :"),
            tr("%1 is not an X!Tandem parameter file")
              .arg(p_tandem_preset_file->getAbsoluteFilePath()));
        }
    }
}
const TandemParametersFile &
EditTandemPresetDialog::getTandemParametersFile() const
{
  if(_p_tandem_preset_file == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("_p_tandem_preset_file == nullptr"));
    }
  if(!_p_tandem_preset_file->exists())
    {
      throw pappso::PappsoException(
        QObject::tr("preset file %1 does not exists")
          .arg(_p_tandem_preset_file->getAbsoluteFilePath()));
    }
  return *_p_tandem_preset_file;
}
void
EditTandemPresetDialog::fillPresetComboBox()
{
  ui->preset_combo_box->clear();
  QString default_preset_name = _tandem_params.getMethodName();

  QDir preset_dir(_preset_directory);
  QStringList filters;
  filters << "*.xml";
  preset_dir.setNameFilters(filters);
  for(QFileInfo file_info : preset_dir.entryInfoList())
    {
      ui->preset_combo_box->addItem(file_info.baseName(),
                                    QVariant(file_info.absoluteFilePath()));
    }
  int index = ui->preset_combo_box->findText(default_preset_name);
  if(index != -1)
    { // -1 for not found
      ui->pushButton_5->setEnabled(true);
      ui->preset_combo_box->setCurrentIndex(index);
    }
}

void
EditTandemPresetDialog::doResultType()
{
  if(ui->oresu_all_radio_button->isChecked() ||
     ui->oresu_stochastic_radio_button->isChecked())
    {
      ui->omvev_edit->setDisabled(true);
      ui->omvpev_edit->setDisabled(true);
    }
  else
    {
      ui->omvev_edit->setDisabled(false);
      ui->omvpev_edit->setDisabled(false);
    }
}

void
EditTandemPresetDialog::doRefine()
{
  if(ui->refine_no_radio_button->isChecked())
    {
      ui->rmvev_edit->setDisabled(true);
      ui->refpntm_edit->setDisabled(true);
      ui->refpctm_edit->setDisabled(true);
      ui->refmm_edit->setDisabled(true);
      ui->refmm1_edit->setDisabled(true);
      ui->refmm2_edit->setDisabled(true);
      ui->refpmm_edit->setDisabled(true);
      ui->refpmm1_edit->setDisabled(true);
      ui->refpmm2_edit->setDisabled(true);
      ui->refpmmotif_edit->setDisabled(true);
      ui->refpmmotif1_edit->setDisabled(true);
      ui->refpmmotif2_edit->setDisabled(true);
      ui->rupmffr_no_radio_button->setDisabled(true);
      ui->rupmffr_yes_radio_button->setDisabled(true);
      ui->rcsemi_no_radio_button->setDisabled(true);
      ui->rcsemi_yes_radio_button->setDisabled(true);
      ui->ruc_yes_radio_button->setDisabled(true);
      ui->ruc_no_radio_button->setDisabled(true);
      ui->rss_yes_radio_button->setDisabled(true);
      ui->rss_no_radio_button->setDisabled(true);
      ui->rpm_yes_radio_button->setDisabled(true);
      ui->rpm_no_radio_button->setDisabled(true);
    }
  else
    {
      ui->rmvev_edit->setDisabled(false);
      ui->refpntm_edit->setDisabled(false);
      ui->refpctm_edit->setDisabled(false);
      ui->refmm_edit->setDisabled(false);
      ui->refmm1_edit->setDisabled(false);
      ui->refmm2_edit->setDisabled(false);
      ui->refpmm_edit->setDisabled(false);
      ui->refpmm1_edit->setDisabled(false);
      ui->refpmm2_edit->setDisabled(false);
      ui->refpmmotif_edit->setDisabled(false);
      ui->refpmmotif1_edit->setDisabled(false);
      ui->refpmmotif2_edit->setDisabled(false);
      ui->rupmffr_no_radio_button->setDisabled(false);
      ui->rupmffr_yes_radio_button->setDisabled(false);
      ui->rcsemi_no_radio_button->setDisabled(false);
      ui->rcsemi_yes_radio_button->setDisabled(false);
      ui->ruc_yes_radio_button->setDisabled(false);
      ui->ruc_no_radio_button->setDisabled(false);
      ui->rss_yes_radio_button->setDisabled(false);
      ui->rss_no_radio_button->setDisabled(false);
      ui->rpm_yes_radio_button->setDisabled(false);
      ui->rpm_no_radio_button->setDisabled(false);
    }
}


void
EditTandemPresetDialog::doEdit(QString value)
{
  QObject *senderObj    = sender();
  QString senderObjName = senderObj->objectName();
  qDebug() << "EditTandemPresetDialog::doEdit begin " << senderObjName << " "
           << value;
  qDebug() << "EditTandemPresetDialog::doEdit end " << senderObjName;
}
void
EditTandemPresetDialog::doHelp()
{
  QObject *senderObj    = sender();
  QString senderObjName = senderObj->objectName();
  qDebug() << "EditTandemPresetDialog::doHelp begin " << senderObjName;
  QFile html_doc;
  if(senderObjName == "spmmeu_push_button")
    {
      // spmmeu.html
      html_doc.setFileName(":/tandem/resources/html_doc/spmmeu.html");
    }
  if(senderObjName == "smpc_push_button")
    {
      // spmmeu.html
      html_doc.setFileName(":/tandem/resources/html_doc/smpc.html");
    }
  if(senderObjName == "spmmem_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/spmmem.html");
    }
  if(senderObjName == "spmmep_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/spmmep.html");
    }
  if(senderObjName == "spmmie_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/spmmie.html");
    }
  if(senderObjName == "sfmt_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sfmt.html");
    }
  if(senderObjName == "sfmmeu_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sfmmeu.html");
    }
  if(senderObjName == "sfmme_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sfmme.html");
    }
  if(senderObjName == "sunlw_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sunlw.html");
    }
  if(senderObjName == "snlm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/snlm.html");
    }
  if(senderObjName == "snlw_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/snlw.html");
    }
  if(senderObjName == "sdr_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sdr.html");
    }
  if(senderObjName == "stp_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/stp.html");
    }
  if(senderObjName == "smp_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/smp.html");
    }
  if(senderObjName == "smfmz_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/smfmz.html");
    }
  if(senderObjName == "smpmh_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/smpmh.html");
    }
  if(senderObjName == "spsbs_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/spsbs.html");
    }
  if(senderObjName == "suca_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/suca.html");
    }
  if(senderObjName == "st_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/st.html");
    }
  if(senderObjName == "pcs_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pcs.html");
    }
  if(senderObjName == "pcsemi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pcsemi.html");
    }

  if(senderObjName == "pcctmc_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pcctmc.html");
    }
  if(senderObjName == "pcntmc_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pcntmc.html");
    }
  if(senderObjName == "pctrmm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pctrmm.html");
    }
  if(senderObjName == "pntrmm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pntrmm.html");
    }
  if(senderObjName == "pqa_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pqa.html");
    }
  if(senderObjName == "pqp_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pqp.html");
    }
  if(senderObjName == "pstpb_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pstpb.html");
    }
  if(senderObjName == "pmrmf_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pmrmf.html");
    }
  if((senderObjName == "rmm_push_button") ||
     (senderObjName == "rmm1_push_button") ||
     (senderObjName == "rmm2_push_button"))
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rmm.html");
    }
  if(senderObjName == "rpmm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rpmm.html");
    }
  if(senderObjName == "rpmmotif_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rpmmotif.html");
    }
  if(senderObjName == "smic_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/smic.html");
    }
  if(senderObjName == "smmcs_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/smmcs.html");
    }
  if(senderObjName == "scp_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/scp.html");
    }
  if(senderObjName == "sir_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sir.html");
    }
  if(senderObjName == "syi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/syi.html");
    }
  if(senderObjName == "sbi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sbi.html");
    }
  if(senderObjName == "sci_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sci.html");
    }
  if(senderObjName == "szi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/szi.html");
    }
  if(senderObjName == "sai_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sai.html");
    }
  if(senderObjName == "sxi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sxi.html");
    }
  if(senderObjName == "refine_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refine.html");
    }
  if(senderObjName == "rmvev_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refmvev.html");
    }
  if(senderObjName == "refpntm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refpntm.html");
    }
  if(senderObjName == "refpctm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refpctm.html");
    }
  if((senderObjName == "refmm_push_button") ||
     (senderObjName == "refmm1_push_button") ||
     (senderObjName == "refmm2_push_button"))
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refmm.html");
    }
  if((senderObjName == "refpmm_push_button") ||
     (senderObjName == "refpmm1_push_button") ||
     (senderObjName == "refpmm2_push_button"))
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refpmm.html");
    }
  if((senderObjName == "refpmmotif_push_button") ||
     (senderObjName == "refpmmotif1_push_button") ||
     (senderObjName == "refpmmotif2_push_button"))
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refpmmotif.html");
    }
  if(senderObjName == "rupmffr_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rupmffr.html");
    }
  if(senderObjName == "rcsemi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rcsemi.html");
    }
  if(senderObjName == "ruc_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/ruc.html");
    }
  if(senderObjName == "rss_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rss.html");
    }
  if(senderObjName == "rpm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rpm.html");
    }
  if((senderObjName == "omvev_push_button") ||
     (senderObjName == "omvpev_push_button"))
    {
      html_doc.setFileName(":/tandem/resources/html_doc/omvev.html");
    }
  if(senderObjName == "oresu_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oresu.html");
    }
  if(senderObjName == "osrb_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/osrb.html");
    }
  if(senderObjName == "oprot_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oprot.html");
    }
  if(senderObjName == "oseq_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oseq.html");
    }
  if(senderObjName == "oosc_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oosc.html");
    }
  if(senderObjName == "ospec_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/ospec.html");
    }
  if(senderObjName == "opara_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/opara.html");
    }
  if(senderObjName == "ohist_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/ohist.html");
    }
  if(senderObjName == "ohcw_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/ohcw.html");
    }
  if(senderObjName == "oph_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oph.html");
    }
  if(senderObjName == "oxp_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oxp.html");
    }
  if(senderObjName == "ttfparam_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/ttfparam.html");
    }

  if(html_doc.open(QFile::ReadOnly | QFile::Text))
    {
      QTextStream in(&html_doc);
      ui->doc_plain_text_edit->setHtml(in.readAll());
      qDebug() << "EditTandemPresetDialog::doHelp doc " << in.readAll();
    }
  else
    {
      qDebug() << "EditTandemPresetDialog::doHelp doc not found";
    }
  qDebug() << "EditTandemPresetDialog::doHelp end " << senderObjName;
}

void
EditTandemPresetDialog::readUi()
{
  try
    {
      // remove the loaded from the method name in the label
      QString temp_preset_name = ui->method_name_label->text();
      temp_preset_name.truncate(temp_preset_name.lastIndexOf(" loaded"));
      _tandem_params.setMethodName(temp_preset_name);

      _tandem_params.setParamLabelValue(
        "spectrum, parent monoisotopic mass error units", "Daltons");
      if(ui->spmmeu_ppm_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue(
            "spectrum, parent monoisotopic mass error units", "ppm");
        }

      _tandem_params.setParamLabelValue(
        "spectrum, parent monoisotopic mass error minus",
        ui->parent_ion_lower_window_edit->text());

      _tandem_params.setParamLabelValue(
        "spectrum, parent monoisotopic mass error plus",
        ui->parent_ion_upper_window_edit->text());

      _tandem_params.setParamLabelValue(
        "spectrum, parent monoisotopic mass isotope error", "no");
      if(ui->spmmie_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue(
            "spectrum, parent monoisotopic mass isotope error", "yes");
        }

      _tandem_params.setParamLabelValue("spectrum, maximum parent charge",
                                        ui->smpc_edit->text());

      _tandem_params.setParamLabelValue("spectrum, fragment mass type",
                                        ui->sfmt_combo_box->currentText());

      _tandem_params.setParamLabelValue(
        "spectrum, fragment monoisotopic mass error units", "Daltons");
      if(ui->sfmmeu_ppm_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue(
            "spectrum, fragment monoisotopic mass error units", "ppm");
        }

      _tandem_params.setParamLabelValue(
        "spectrum, fragment monoisotopic mass error", ui->sfmme_edit->text());
      _tandem_params.setParamLabelValue("spectrum, use neutral loss window",
                                        "no");
      if(ui->sunlw_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("spectrum, use neutral loss window",
                                            "yes");
        }

      _tandem_params.setParamLabelValue("spectrum, neutral loss mass",
                                        ui->snlm_edit->text());
      _tandem_params.setParamLabelValue("spectrum, neutral loss window",
                                        ui->snlw_edit->text());
      _tandem_params.setParamLabelValue("spectrum, dynamic range",
                                        ui->sdr_edit->text());
      _tandem_params.setParamLabelValue("spectrum, total peaks",
                                        ui->stp_edit->text());
      _tandem_params.setParamLabelValue("spectrum, minimum peaks",
                                        ui->smp_edit->text());
      _tandem_params.setParamLabelValue("spectrum, minimum fragment mz",
                                        ui->smfmz_edit->text());
      _tandem_params.setParamLabelValue("spectrum, minimum parent m+h",
                                        ui->smpmh_edit->text());
      _tandem_params.setParamLabelValue("spectrum, sequence batch size",
                                        ui->spsbs_edit->text());
      _tandem_params.setParamLabelValue("spectrum, use contrast angle", "no");
      if(ui->suca_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("spectrum, use contrast angle",
                                            "yes");
        }
      _tandem_params.setParamLabelValue("spectrum, threads",
                                        ui->st_edit->text());

      _tandem_params.setParamLabelValue("spectrum, timstof MS2 filters",
                                        ui->ttfparam_edit->text());

      _tandem_params.setParamLabelValue("protein, cleavage site",
                                        ui->pcs_edit->text());
      _tandem_params.setParamLabelValue("protein, cleavage semi", "no");
      if(ui->pcsemi_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("protein, cleavage semi", "yes");
        }
      _tandem_params.setParamLabelValue(
        "protein, cleavage C-terminal mass change", ui->pcctmc_edit->text());
      _tandem_params.setParamLabelValue(
        "protein, cleavage N-terminal mass change", ui->pcntmc_edit->text());
      _tandem_params.setParamLabelValue("protein, quick acetyl", "no");
      if(ui->pqa_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("protein, quick acetyl", "yes");
        }
      _tandem_params.setParamLabelValue("protein, quick pyrolidone", "no");
      if(ui->pqp_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("protein, quick pyrolidone", "yes");
        }
      _tandem_params.setParamLabelValue("protein, stP bias", "no");
      if(ui->pstpb_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("protein, stP bias", "yes");
        }
      _tandem_params.setParamLabelValue("protein, modified residue mass file",
                                        ui->pmrmf_edit->text());


      _tandem_params.setParamLabelValue("residue, modification mass",
                                        ui->rmm_edit->text());
      _tandem_params.setParamLabelValue("residue, modification mass 1",
                                        ui->rmm1_edit->text());
      _tandem_params.setParamLabelValue("residue, modification mass 2",
                                        ui->rmm2_edit->text());
      _tandem_params.setParamLabelValue("residue, potential modification mass",
                                        ui->rpmm_edit->text());

      _tandem_params.setParamLabelValue("residue, potential modification motif",
                                        ui->rpmmotif_edit->text());
      _tandem_params.setParamLabelValue(
        "scoring, maximum missed cleavage sites", ui->smmcs_edit->text());
      _tandem_params.setParamLabelValue("scoring, cyclic permutation", "no");
      if(ui->scp_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("scoring, cyclic permutation",
                                            "yes");
        }
      _tandem_params.setParamLabelValue("scoring, include reverse", "no");
      if(ui->sir_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("scoring, include reverse", "yes");
        }
      _tandem_params.setParamLabelValue("scoring, minimum ion count",
                                        ui->smic_edit->text());

      _tandem_params.setParamLabelValue("scoring, y ions", "no");
      if(ui->syi_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("scoring, y ions", "yes");
        }
      _tandem_params.setParamLabelValue("scoring, b ions", "no");
      if(ui->sbi_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("scoring, b ions", "yes");
        }
      _tandem_params.setParamLabelValue("scoring, c ions", "no");
      if(ui->sci_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("scoring, c ions", "yes");
        }
      _tandem_params.setParamLabelValue("scoring, z ions", "no");
      if(ui->szi_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("scoring, z ions", "yes");
        }
      _tandem_params.setParamLabelValue("scoring, a ions", "no");
      if(ui->sai_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("scoring, a ions", "yes");
        }
      _tandem_params.setParamLabelValue("scoring, x ions", "no");
      if(ui->sxi_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("scoring, x ions", "yes");
        }
      _tandem_params.setParamLabelValue("refine", "no");
      if(ui->refine_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("refine", "yes");
        }
      _tandem_params.setParamLabelValue(
        "refine, maximum valid expectation value", ui->rmvev_edit->text());

      _tandem_params.setParamLabelValue(
        "refine, potential N-terminus modifications", ui->refpntm_edit->text());
      _tandem_params.setParamLabelValue(
        "refine, potential C-terminus modifications", ui->refpctm_edit->text());
      _tandem_params.setParamLabelValue("refine, modification mass",
                                        ui->refmm_edit->text());
      _tandem_params.setParamLabelValue("refine, modification mass 1",
                                        ui->refmm1_edit->text());
      _tandem_params.setParamLabelValue("refine, modification mass 2",
                                        ui->refmm2_edit->text());
      _tandem_params.setParamLabelValue("refine, potential modification mass",
                                        ui->refpmm_edit->text());
      _tandem_params.setParamLabelValue("refine, potential modification mass 1",
                                        ui->refpmm1_edit->text());
      _tandem_params.setParamLabelValue("refine, potential modification mass 2",
                                        ui->refpmm2_edit->text());
      _tandem_params.setParamLabelValue("refine, potential modification motif",
                                        ui->refpmmotif_edit->text());
      _tandem_params.setParamLabelValue(
        "refine, potential modification motif 1", ui->refpmmotif1_edit->text());
      _tandem_params.setParamLabelValue(
        "refine, potential modification motif 2", ui->refpmmotif2_edit->text());
      _tandem_params.setParamLabelValue(
        "refine, use potential modifications for full refinement", "no");
      if(ui->rupmffr_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue(
            "refine, use potential modifications for full refinement", "yes");
        }
      _tandem_params.setParamLabelValue("refine, cleavage semi", "no");
      if(ui->rcsemi_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("refine, cleavage semi", "yes");
        }
      _tandem_params.setParamLabelValue("refine, unanticipated cleavage", "no");
      if(ui->ruc_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("refine, unanticipated cleavage",
                                            "yes");
        }
      _tandem_params.setParamLabelValue("refine, spectrum synthesis", "no");
      if(ui->rss_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("refine, spectrum synthesis",
                                            "yes");
        }
      _tandem_params.setParamLabelValue("refine, point mutations", "no");
      if(ui->rpm_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("refine, point mutations", "yes");
        }

      _tandem_params.setParamLabelValue(
        "output, maximum valid expectation value", ui->omvev_edit->text());

      _tandem_params.setParamLabelValue(
        "output, maximum valid protein expectation value",
        ui->omvpev_edit->text());


      _tandem_params.setParamLabelValue("output, results", "valid");
      if(ui->oresu_all_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("output, results", "all");
          // ui->omvev_edit->setDisabled(true);
          // ui->omvpev_edit->setDisabled(true);
        }
      if(ui->oresu_stochastic_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("output, results", "stochastic");
        }
      _tandem_params.setParamLabelValue("output, spectra", "no");
      if(ui->ospec_yes_radio_button->isChecked())
        {
          _tandem_params.setParamLabelValue("output, spectra", "yes");
        }
      _tandem_params.setParamLabelValue("output, histogram column width", "30");

      _tandem_params.setParamLabelValue("output, xsl path",
                                        ui->oxp_edit->text());
    }
  catch(pappso::PappsoException &error)
    {
      QMessageBox::warning(this, tr("Error in parameters :"), error.qwhat());
    }
}

void
EditTandemPresetDialog::populate()
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  try
    {
      if(!_tandem_params.getMethodName().isEmpty())
        {
          ui->method_name_label->setText(
            tr("%1 loaded").arg(_tandem_params.getMethodName()));
        }
      /*
       * <note type="input" label="spectrum, parent monoisotopic mass error
       * units">ppm</note>
       * */
      ui->spmmeu_daltons_radio_button->setChecked(true);
      ui->spmmeu_ppm_radio_button->setChecked(false);
      if(_tandem_params.getValue(
           "spectrum, parent monoisotopic mass error units") == "ppm")
        { // -1 for not found
          ui->spmmeu_daltons_radio_button->setChecked(false);
          ui->spmmeu_ppm_radio_button->setChecked(true);
        }
      //<note type="input" label="spectrum, parent monoisotopic mass error
      // minus">10</note>
      ui->parent_ion_lower_window_edit->setText(_tandem_params.getValue(
        "spectrum, parent monoisotopic mass error minus"));
      //<note type="input" label="spectrum, parent monoisotopic mass error
      // plus">10</note>
      ui->parent_ion_upper_window_edit->setText(_tandem_params.getValue(
        "spectrum, parent monoisotopic mass error plus"));
      //<note type="input" label="spectrum, parent monoisotopic mass isotope
      // error">yes</note>
      ui->spmmie_yes_radio_button->setChecked(true);
      ui->spmmie_no_radio_button->setChecked(false);
      if(_tandem_params.getValue(
           "spectrum, parent monoisotopic mass isotope error") == "no")
        { // -1 for not found
          ui->spmmie_yes_radio_button->setChecked(false);
          ui->spmmie_no_radio_button->setChecked(true);
        }
      //<note type="input" label="spectrum, maximum parent charge">4</note>
      ui->smpc_edit->setText(
        _tandem_params.getValue("spectrum, maximum parent charge"));

      //<note type="input" label="spectrum, fragment mass
      // type">monoisotopic</note>
      auto index = ui->sfmt_combo_box->findText("monoisotopic");
      if(index != -1)
        { // -1 for not found
          ui->sfmt_combo_box->setCurrentIndex(index);
        }
      //<note type="input" label="spectrum, fragment monoisotopic mass error
      // units">Daltons</note>
      ui->sfmmeu_daltons_radio_button->setChecked(true);
      ui->sfmmeu_ppm_radio_button->setChecked(false);
      if(_tandem_params.getValue(
           "spectrum, fragment monoisotopic mass error units") == "ppm")
        { // -1 for not found
          ui->sfmmeu_daltons_radio_button->setChecked(false);
          ui->sfmmeu_ppm_radio_button->setChecked(true);
        }

      //<note type="input" label="spectrum, fragment monoisotopic mass
      // error">0.02</note>
      ui->sfmme_edit->setText(
        _tandem_params.getValue("spectrum, fragment monoisotopic mass error"));


      //<note type="input" label="spectrum, use neutral loss window">yes</note>
      ui->sunlw_yes_radio_button->setChecked(true);
      ui->sunlw_no_radio_button->setChecked(false);
      try
        {
          if(_tandem_params.getValue("spectrum, use neutral loss window") ==
             "no")
            { // -1 for not found
              ui->sunlw_yes_radio_button->setChecked(false);
              ui->sunlw_no_radio_button->setChecked(true);
            }
        }
      catch(pappso::ExceptionNotFound &error)
        {
          ui->sunlw_yes_radio_button->setChecked(false);
          ui->sunlw_no_radio_button->setChecked(true);
        }


      try
        {
          //<note type="input" label="spectrum, neutral loss
          // mass">18.01057</note>
          ui->snlm_edit->setText(
            _tandem_params.getValue("spectrum, neutral loss mass"));
        }
      catch(pappso::ExceptionNotFound &error)
        {
          ui->snlm_edit->setText("18.01057");
        }

      try
        {
          //<note type="input" label="spectrum, neutral loss window">0.02</note>
          ui->snlw_edit->setText(
            _tandem_params.getValue("spectrum, neutral loss window"));
        }
      catch(pappso::ExceptionNotFound &error)
        {
          ui->snlm_edit->setText("0.02");
        }

      //<note type="input" label="spectrum, dynamic range">100.0</note>
      ui->sdr_edit->setText(_tandem_params.getValue("spectrum, dynamic range"));

      //<note type="input" label="spectrum, total peaks">100</note>
      ui->stp_edit->setText(_tandem_params.getValue("spectrum, total peaks"));

      ui->smp_edit->setText(_tandem_params.getValue("spectrum, minimum peaks"));


      //<note type="input" label="spectrum, minimum fragment mz">150.0</note>
      ui->smfmz_edit->setText(
        _tandem_params.getValue("spectrum, minimum fragment mz"));
      //<note type="input" label="spectrum, minimum parent m+h">500.0</note>
      ui->smpmh_edit->setText(
        _tandem_params.getValue("spectrum, minimum parent m+h"));

      //<note type="input" label="spectrum, sequence batch size">1000</note>
      ui->spsbs_edit->setText(
        _tandem_params.getValue("spectrum, sequence batch size"));
      //<note type="input" label="spectrum, use contrast angle">no</note>
      ui->suca_yes_radio_button->setChecked(true);
      ui->suca_no_radio_button->setChecked(false);

      try
        {
          if(_tandem_params.getValue("spectrum, use contrast angle") == "no")
            { // -1 for not found
              ui->suca_yes_radio_button->setChecked(false);
              ui->suca_no_radio_button->setChecked(true);
            }
        }
      catch(pappso::ExceptionNotFound &error)
        {

          ui->suca_yes_radio_button->setChecked(false);
          ui->suca_no_radio_button->setChecked(true);
        }

      //<note type="input" label="spectrum, threads">1</note>
      ui->st_edit->setText(_tandem_params.getValue("spectrum, threads"));

      ui->ttfparam_edit->setText(
        _tandem_params.getValue("spectrum, timstof MS2 filters"));

      //<note type="input" label="protein, cleavage site">[RK]|{P}</note>
      ui->pcs_edit->setText(_tandem_params.getValue("protein, cleavage site"));
      //<note type="input" label="protein, cleavage semi">no</note>
      ui->pcsemi_yes_radio_button->setChecked(true);
      ui->pcsemi_no_radio_button->setChecked(false);
      if(_tandem_params.getValue("protein, cleavage semi") == "no")
        { // -1 for not found
          ui->pcsemi_yes_radio_button->setChecked(false);
          ui->pcsemi_no_radio_button->setChecked(true);
        }


      try
        {
          //<note type="input" label="protein, cleavage C-terminal mass
          // change">+17.00305</note>
          ui->pcctmc_edit->setText(_tandem_params.getValue(
            "protein, cleavage C-terminal mass change"));
        }
      catch(pappso::ExceptionNotFound &error)
        {

          ui->pcctmc_edit->setText("+17.00305");
        }

      try
        {
          //<note type="input" label="protein, cleavage N-terminal mass
          // change">+1.00794</note>
          ui->pcntmc_edit->setText(_tandem_params.getValue(
            "protein, cleavage N-terminal mass change"));
        }
      catch(pappso::ExceptionNotFound &error)
        {

          ui->pcntmc_edit->setText("+1.00794");
        }

      //<note type="input" label="protein, C-terminal residue modification
      // mass">0.0</note>
      ui->pctrmm_edit->setText(_tandem_params.getValue(
        "protein, C-terminal residue modification mass"));
      //<note type="input" label="protein, N-terminal residue modification
      // mass">0.0</note>
      ui->pntrmm_edit->setText(_tandem_params.getValue(
        "protein, N-terminal residue modification mass"));

      //<note type="input" label="protein, quick acetyl">yes</note>
      ui->pqa_yes_radio_button->setChecked(true);
      ui->pqa_no_radio_button->setChecked(false);
      if(_tandem_params.getValue("protein, quick acetyl") == "no")
        { // -1 for not found
          ui->pqa_yes_radio_button->setChecked(false);
          ui->pqa_no_radio_button->setChecked(true);
        }

      //<note type="input" label="protein, quick pyrolidone">yes</note>
      ui->pqp_yes_radio_button->setChecked(true);
      ui->pqp_no_radio_button->setChecked(false);
      if(_tandem_params.getValue("protein, quick pyrolidone") == "no")
        { // -1 for not found
          ui->pqp_yes_radio_button->setChecked(false);
          ui->pqp_no_radio_button->setChecked(true);
        }


      //<note type="input" label="protein, stP bias">yes</note>
      ui->pstpb_yes_radio_button->setChecked(true);
      ui->pstpb_no_radio_button->setChecked(false);
      try
        {
          if(_tandem_params.getValue("protein, stP bias") == "no")
            { // -1 for not found
              ui->pstpb_yes_radio_button->setChecked(false);
              ui->pstpb_no_radio_button->setChecked(true);
            }
        }
      catch(pappso::ExceptionNotFound &error)
        {
        }

      //<note type="input" label="protein, modified residue mass file"></note>
      ui->pmrmf_edit->setText(
        _tandem_params.getValue("protein, modified residue mass file"));

      //<note type="input" label="residue, modification mass">57.02146@C</note>
      ui->rmm_edit->setText(
        _tandem_params.getValue("residue, modification mass"));

      try
        {
          //<note type="input" label="residue, modification mass 1"></note>
          ui->rmm1_edit->setText(
            _tandem_params.getValue("residue, modification mass 1"));
        }
      catch(pappso::ExceptionNotFound &error)
        {
          ui->rmm1_edit->setText("");
        }

      try
        {
          //<note type="input" label="residue, modification mass 2"></note>
          ui->rmm2_edit->setText(
            _tandem_params.getValue("residue, modification mass 2"));
        }
      catch(pappso::ExceptionNotFound &error)
        {
          ui->rmm2_edit->setText("");
        }

      //<note type="input" label="residue, potential modification
      // mass">15.99491@M</note>
      ui->rpmm_edit->setText(
        _tandem_params.getValue("residue, potential modification mass"));
      //<note type="input" label="residue, potential modification motif"></note>
      ui->rpmmotif_edit->setText(
        _tandem_params.getValue("residue, potential modification motif"));
      //<note type="input" label="scoring, minimum ion count">4</note>
      ui->smic_edit->setText(
        _tandem_params.getValue("scoring, minimum ion count"));
      //<note type="input" label="scoring, maximum missed cleavage
      // sites">1</note>
      ui->smmcs_edit->setText(
        _tandem_params.getValue("scoring, maximum missed cleavage sites"));
      //<note type="input" label="scoring, cyclic permutation">yes</note>
      ui->scp_yes_radio_button->setChecked(true);
      ui->scp_no_radio_button->setChecked(false);
      if(_tandem_params.getValue("scoring, cyclic permutation") == "no")
        { // -1 for not found
          ui->scp_yes_radio_button->setChecked(false);
          ui->scp_no_radio_button->setChecked(true);
        }
      //<note type="input" label="scoring, include reverse">yes</note>
      ui->sir_yes_radio_button->setChecked(true);
      ui->sir_no_radio_button->setChecked(false);
      if(_tandem_params.getValue("scoring, include reverse") == "no")
        { // -1 for not found
          ui->sir_yes_radio_button->setChecked(false);
          ui->sir_no_radio_button->setChecked(true);
        }
      //<note type="input" label="scoring, y ions">yes</note>
      ui->syi_yes_radio_button->setChecked(true);
      ui->syi_no_radio_button->setChecked(false);
      if(_tandem_params.getValue("scoring, y ions") == "no")
        { // -1 for not found
          ui->syi_yes_radio_button->setChecked(false);
          ui->syi_no_radio_button->setChecked(true);
        }
      //<note type="input" label="scoring, b ions">yes</note>
      ui->sbi_yes_radio_button->setChecked(true);
      ui->sbi_no_radio_button->setChecked(false);
      if(_tandem_params.getValue("scoring, b ions") == "no")
        { // -1 for not found
          ui->sbi_yes_radio_button->setChecked(false);
          ui->sbi_no_radio_button->setChecked(true);
        }
      //<note type="input" label="scoring, c ions">no</note>
      ui->sci_yes_radio_button->setChecked(true);
      ui->sci_no_radio_button->setChecked(false);
      if(_tandem_params.getValue("scoring, c ions") == "no")
        { // -1 for not found
          ui->sci_yes_radio_button->setChecked(false);
          ui->sci_no_radio_button->setChecked(true);
        }
      //<note type="input" label="scoring, z ions">no</note>
      ui->szi_yes_radio_button->setChecked(true);
      ui->szi_no_radio_button->setChecked(false);
      if(_tandem_params.getValue("scoring, z ions") == "no")
        { // -1 for not found
          ui->szi_yes_radio_button->setChecked(false);
          ui->szi_no_radio_button->setChecked(true);
        }
      //<note type="input" label="scoring, a ions">no</note>
      ui->sai_yes_radio_button->setChecked(true);
      ui->sai_no_radio_button->setChecked(false);
      if(_tandem_params.getValue("scoring, a ions") == "no")
        { // -1 for not found
          ui->sai_yes_radio_button->setChecked(false);
          ui->sai_no_radio_button->setChecked(true);
        }
      //<note type="input" label="scoring, x ions">no</note>
      ui->sxi_yes_radio_button->setChecked(true);
      ui->sxi_no_radio_button->setChecked(false);
      if(_tandem_params.getValue("scoring, x ions") == "no")
        { // -1 for not found
          ui->sxi_yes_radio_button->setChecked(false);
          ui->sxi_no_radio_button->setChecked(true);
        }


      //<note type="input" label="refine">yes</note>
      ui->refine_yes_radio_button->setChecked(true);
      ui->refine_no_radio_button->setChecked(false);
      if(_tandem_params.getValue("refine") == "no")
        { // -1 for not found
          ui->refine_yes_radio_button->setChecked(false);
          ui->refine_no_radio_button->setChecked(true);
          ui->rmvev_edit->setDisabled(true);
          ui->refpntm_edit->setDisabled(true);
          ui->refpctm_edit->setDisabled(true);
          ui->refmm_edit->setDisabled(true);
          ui->refmm1_edit->setDisabled(true);
          ui->refmm2_edit->setDisabled(true);
          ui->refpmm_edit->setDisabled(true);
          ui->refpmm1_edit->setDisabled(true);
          ui->refpmm2_edit->setDisabled(true);
          ui->refpmmotif_edit->setDisabled(true);
          ui->refpmmotif1_edit->setDisabled(true);
          ui->refpmmotif2_edit->setDisabled(true);
          ui->rupmffr_no_radio_button->setDisabled(true);
          ui->rupmffr_yes_radio_button->setDisabled(true);
          ui->rcsemi_no_radio_button->setDisabled(true);
          ui->rcsemi_yes_radio_button->setDisabled(true);
          ui->ruc_yes_radio_button->setDisabled(true);
          ui->ruc_no_radio_button->setDisabled(true);
          ui->rss_yes_radio_button->setDisabled(true);
          ui->rss_no_radio_button->setDisabled(true);
          ui->rpm_yes_radio_button->setDisabled(true);
          ui->rpm_no_radio_button->setDisabled(true);
        }
      //<note type="input" label="refine, maximum valid expectation
      // value">0.01</note>
      ui->rmvev_edit->setText(
        _tandem_params.getValue("refine, maximum valid expectation value"));

      //<note type="input" label="refine, potential N-terminus
      // modifications">+42.01056@[</note>
      ui->refpntm_edit->setText(
        _tandem_params.getValue("refine, potential N-terminus modifications"));
      //<note type="input" label="refine, potential C-terminus
      // modifications"></note>
      ui->refpctm_edit->setText(
        _tandem_params.getValue("refine, potential C-terminus modifications"));
      //<note type="input" label="refine, modification mass">57.02146@C</note>
      ui->refmm_edit->setText(
        _tandem_params.getValue("refine, modification mass"));
      try
        {
          //<note type="input" label="refine, modification mass 1"></note>
          ui->refmm1_edit->setText(
            _tandem_params.getValue("refine, modification mass 1"));
        }
      catch(pappso::ExceptionNotFound &error)
        {
          ui->refmm1_edit->setText("");
        }

      try
        {
          //<note type="input" label="refine, modification mass 2"></note>
          ui->refmm2_edit->setText(
            _tandem_params.getValue("refine, modification mass 2"));
        }
      catch(pappso::ExceptionNotFound &error)
        {
          ui->refmm2_edit->setText("");
        }
      //<note type="input" label="refine, potential modification
      // mass">15.99491@M</note>
      ui->refpmm_edit->setText(
        _tandem_params.getValue("refine, potential modification mass"));
      try
        {
          //<note type="input" label="refine, potential modification mass
          // 1"></note>
          ui->refpmm1_edit->setText(
            _tandem_params.getValue("refine, potential modification mass 1"));
        }
      catch(pappso::ExceptionNotFound &error)
        {
          ui->refpmm1_edit->setText("");
        }
      try
        {
          //<note type="input" label="refine, potential modification mass
          // 2"></note>
          ui->refpmm2_edit->setText(
            _tandem_params.getValue("refine, potential modification mass 2"));
        }
      catch(pappso::ExceptionNotFound &error)
        {
          ui->refpmm2_edit->setText("");
        }
      //<note type="input" label="refine, potential modification motif"></note>
      ui->refpmmotif_edit->setText(
        _tandem_params.getValue("refine, potential modification motif"));
      try
        {
          //<note type="input" label="refine, potential modification motif
          // 1"></note>
          ui->refpmmotif1_edit->setText(
            _tandem_params.getValue("refine, potential modification motif 1"));
        }
      catch(pappso::ExceptionNotFound &error)
        {
          ui->refpmmotif1_edit->setText("");
        }
      try
        {
          //<note type="input" label="refine, potential modification motif
          // 2"></note>
          ui->refpmmotif2_edit->setText(
            _tandem_params.getValue("refine, potential modification motif 2"));
        }
      catch(pappso::ExceptionNotFound &error)
        {
          ui->refpmmotif2_edit->setText("");
        }
      //<note type="input" label="refine, use potential modifications for full
      // refinement">yes</note>
      ui->rupmffr_yes_radio_button->setChecked(true);
      ui->rupmffr_no_radio_button->setChecked(false);
      if(_tandem_params.getValue(
           "refine, use potential modifications for full refinement") == "no")
        { // -1 for not found
          ui->rupmffr_yes_radio_button->setChecked(false);
          ui->rupmffr_no_radio_button->setChecked(true);
        }
      //<note type="input" label="refine, cleavage semi">no</note>
      ui->rcsemi_yes_radio_button->setChecked(true);
      ui->rcsemi_no_radio_button->setChecked(false);
      try
        {
          if(_tandem_params.getValue("refine, cleavage semi") == "no")
            { // -1 for not found
              ui->rcsemi_yes_radio_button->setChecked(false);
              ui->rcsemi_no_radio_button->setChecked(true);
            }
        }
      catch(pappso::ExceptionNotFound &error)
        {
        }
      //<note type="input" label="refine, unanticipated cleavage">no</note>
      ui->ruc_yes_radio_button->setChecked(true);
      ui->ruc_no_radio_button->setChecked(false);
      if(_tandem_params.getValue("refine, unanticipated cleavage") == "no")
        { // -1 for not found
          ui->ruc_yes_radio_button->setChecked(false);
          ui->ruc_no_radio_button->setChecked(true);
        }
      //<note type="input" label="refine, spectrum synthesis">yes</note>
      ui->rss_yes_radio_button->setChecked(true);
      ui->rss_no_radio_button->setChecked(false);
      if(_tandem_params.getValue("refine, spectrum synthesis") == "no")
        { // -1 for not found
          ui->rss_yes_radio_button->setChecked(false);
          ui->rss_no_radio_button->setChecked(true);
        }
      //<note type="input" label="refine, point mutations">no</note>
      ui->rpm_yes_radio_button->setChecked(true);
      ui->rpm_no_radio_button->setChecked(false);
      if(_tandem_params.getValue("refine, point mutations") == "no")
        { // -1 for not found
          ui->rpm_yes_radio_button->setChecked(false);
          ui->rpm_no_radio_button->setChecked(true);
        }


      try
        {
          //<note type="input" label="output, maximum valid expectation
          // value">0.05</note>
          ui->omvev_edit->setText(
            _tandem_params.getValue("output, maximum valid expectation value"));
        }
      catch(pappso::ExceptionNotFound &error)
        {

          ui->omvev_edit->setText("0.01");
        }

      try
        {
          //<note type="input" label="output, maximum valid protein expectation
          // value">0.05</note>
          ui->omvpev_edit->setText(_tandem_params.getValue(
            "output, maximum valid protein expectation value"));
        }
      catch(pappso::ExceptionNotFound &error)
        {

          ui->omvpev_edit->setText("0.01");
        }
      //<note type="input" label="output, results">valid</note>
      ui->oresu_all_radio_button->setChecked(false);
      ui->oresu_valid_radio_button->setChecked(true);
      ui->oresu_stochastic_radio_button->setChecked(false);
      if(_tandem_params.getValue("output, results") == "all")
        { // -1 for not found
          ui->oresu_all_radio_button->setChecked(true);
          ui->oresu_valid_radio_button->setChecked(false);
          ui->oresu_stochastic_radio_button->setChecked(false);
          ui->omvev_edit->setDisabled(true);
          ui->omvpev_edit->setDisabled(true);
        }
      if(_tandem_params.getValue("output, results") == "stochastic")
        { // -1 for not found
          ui->oresu_all_radio_button->setChecked(false);
          ui->oresu_valid_radio_button->setChecked(false);
          ui->oresu_stochastic_radio_button->setChecked(true);
          ui->omvev_edit->setDisabled(true);
          ui->omvpev_edit->setDisabled(true);
        }
      //<note type="input" label="output, sort results by">spectrum</note>
      //<note type="input" label="output, proteins">yes</note>
      //<note type="input" label="output, sequences">yes</note>
      //<note type="input" label="output, one sequence copy">yes</note>
      //<note type="input" label="output, spectra">yes</note>
      ui->ospec_yes_radio_button->setChecked(true);
      ui->ospec_no_radio_button->setChecked(false);
      if(_tandem_params.getValue("output, spectra") == "no")
        { // -1 for not found
          ui->ospec_yes_radio_button->setChecked(false);
          ui->ospec_no_radio_button->setChecked(true);
        }
      //<note type="input" label="output, parameters">yes</note>
      //<note type="input" label="output, performance">yes</note>
      //<note type="input" label="output, histograms">yes</note>
      //<note type="input" label="output, histogram column width">30</note>
      //<note type="input" label="output, path hashing">no</note>
      //<note type="input" label="output, xsl path">tandem-style.xsl</note>
      ui->oxp_edit->setText(_tandem_params.getValue("output, xsl path"));
    }
  catch(pappso::ExceptionNotFound &error)
    {
      QMessageBox::warning(this, tr("Parameter not found :"), error.qwhat());
    }
  catch(pappso::PappsoException &error)
    {
      QMessageBox::warning(this, tr("Error in parameters :"), error.qwhat());
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

void
EditTandemPresetDialog::done(int r)
{
  if(QDialog::Accepted == r) // ok was pressed
    {
      readUi();
      if(!_p_tandem_preset_file->exists())
        {
          int ok = QMessageBox::warning(
            this,
            tr("Save parameters"),
            tr("Parameters file does not exist.\nDo you want to save a file "
               "named %1")
              .arg(_p_tandem_preset_file->getMethodName()),
            QMessageBox::Cancel,
            QMessageBox::Save);

          if(ok == QMessageBox::Cancel)
            {
              return;
            }
          else if(ok == QMessageBox::Save)
            {
              doSave();
              QDialog::done(r);
              return;
            }
        }
      TandemParameters ref_params =
        _p_tandem_preset_file->getTandemParameters();
      if(ref_params.equals(_tandem_params)) // validate the data somehow
        {
          QDialog::done(r);
          return;
        }
      else
        {
          int ok =
            QMessageBox::warning(this,
                                 tr("Save parameters"),
                                 tr("Parameters modified but not saved in the "
                                    "file %1.\nDo you want to save it ?")
                                   .arg(_p_tandem_preset_file->getMethodName()),
                                 QMessageBox::Cancel,
                                 QMessageBox::Save);

          if(ok == QMessageBox::Cancel)
            {
              return;
            }
          else if(ok == QMessageBox::Save)
            {
              doSave();
              QDialog::done(r);
              return;
            }
        }
    }
  else // cancel, close or exc was pressed
    {
      QDialog::done(r);
      return;
    }
}

void
EditTandemPresetDialog::fillPresetDirectoryLabel()
{
  if(!_preset_directory.isEmpty())
    {
      if(QFileInfo(_preset_directory).exists() &&
         QFileInfo(_preset_directory).isDir())
        {
          ui->label_loaded_directory->setText(
            tr("%1 directory selected").arg(_preset_directory));
          ui->preset_combo_box->setEnabled(true);
          ui->pushButton->setEnabled(true);
        }
    }
}
