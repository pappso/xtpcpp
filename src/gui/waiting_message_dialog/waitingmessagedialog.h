
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QDialog>
#include <QStringListModel>
#include <QMovie>
#include <QMutex>
#include "../widgets/automatic_filter_widget/automaticfilterwidget.h"


namespace Ui
{
class WaitingMessageDialog;
}


class WaitingMessageDialog : public QDialog
{
  Q_OBJECT

  public:
  explicit WaitingMessageDialog(QWidget *parent);
  ~WaitingMessageDialog();

  void message(const QString &message);
  void message(const QString &message, int progress_value);
  void appendText(const char *text);
  void appendText(const QString &message);
  void setText(const QString text);
  void showEvent(QShowEvent *event);
  void hideEvent(QHideEvent *event);
  bool stopWorkerThread();
  void showStopButton();
  void percent(int progress_value);


  private:
  private slots:
  // void toggledStopButton(bool pushed);

  private:
  Ui::WaitingMessageDialog *ui;
  QMovie *_p_movie;

  QMutex m_mutex;
};
