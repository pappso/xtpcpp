/**
 * \file gui/ptm_island_list_window/ptmislandtablemodel.h
 * \date 30/5/2017
 * \author Olivier Langella
 * \brief display all ptm islands
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QAbstractTableModel>
#include "../../core/identificationgroup.h"
#include "../../grouping/ptm/ptmgroupingexperiment.h"


/** \def PtmIslandListColumn list of available fields to display in ptm island
 * list
 *
 */

enum class PtmIslandListColumn : std::int8_t
{
  checked           = 0,  ///< checked ptm
  ptm_island_id     = 1,  ///< ptm_island_id
  accession         = 2,  ///< protein accession
  description       = 3,  ///< protein description
  ptm_position_list = 4,  ///< ptm position list
  spectrum          = 5,  ///< count sample scans
  ptm_spectrum      = 6,  ///< count scan with ptm
  sequence          = 7,  ///< unique sequence count
  multiptm          = 8,  ///< count multi ptm peptide match
  ptm_island_start  = 9,  ///< start position of the ptm island on the protein
  ptm_island_length = 10, ///< length of the ptm island
  last              = 11,

};


class PtmIslandListWindow;
class PtmIslandTableModel : public QAbstractTableModel
{
  Q_OBJECT
  public:
  PtmIslandTableModel(PtmIslandListWindow *p_ptm_island_list_window);

  ~PtmIslandTableModel();

  void setIdentificationGroup(IdentificationGroup *p_identification_group);
  IdentificationGroup *getIdentificationGroup();

  virtual int
  rowCount(const QModelIndex &parent = QModelIndex()) const override;
  virtual int
  columnCount(const QModelIndex &parent = QModelIndex()) const override;
  virtual QVariant
  headerData(int section, Qt::Orientation orientation, int role) const override;
  virtual QVariant data(const QModelIndex &index,
                        int role = Qt::DisplayRole) const override;

  static const QString getTitle(PtmIslandListColumn column);
  static const QString getDescription(PtmIslandListColumn column);
  static PtmIslandListColumn getPtmIslandListColumn(std::int8_t column);

  const PtmGroupingExperiment *getPtmGroupingExperiment() const;

  public slots:
  void onPtmIslandDataChanged();

  private:
  static int getColumnWidth(int column);
  static const QString getTitle(std::int8_t column);
  static const QString getDescription(std::int8_t column);

  private:
  IdentificationGroup *_p_identification_group   = nullptr;
  PtmIslandListWindow *_p_ptm_island_list_window = nullptr;
};
