/**
 * \file utils/workmonitor.h
 * \date 10/6/2017
 * \author Olivier Langella
 * \brief monitoring progress in any worker thread
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QObject>
#include <QElapsedTimer>
#include <pappsomspp/processing/uimonitor/uimonitorinterface.h>
#include <QMutex>

class SubMonitor;
class WorkMonitorInterface : public pappso::UiMonitorInterface
{
  public:
  virtual void finished(const QString &message)           = 0;
  virtual void canceled(const QString &message)           = 0;
  virtual void message(const QString &message)            = 0;
  virtual void message(const QString &message, int value) = 0;
  virtual void setText(const QString text)                = 0;
  virtual SubMonitor &getSubMonitor()                     = 0;
};

class MainWindow;


class WorkMonitor : public QObject, public WorkMonitorInterface
{
  Q_OBJECT
  public:
  WorkMonitor(MainWindow *main_window);
  void finished(const QString &message) override;
  void canceled(const QString &message) override;
  void message(const QString &message) override;
  void message(const QString &message, int value) override;
  virtual void setText(const QString text) override;


  virtual bool shouldIstop() override;
  virtual void count() override;
  virtual void setTitle(const QString &title) override;
  virtual void setStatus(const QString &status) override;
  virtual void appendText(const QString &text) override;
  virtual void setTotalSteps(std::size_t total_number_of_steps) override;

  SubMonitor &getSubMonitor() override;

  signals:
  void workerJobFinished(QString message);
  void workerJobCanceled(QString message);
  void workerMessage(QString message);
  void workerMessagePercent(QString message, int value);
  void workerPercent(int value);
  // void workerAppendText(const char *p_char);
  void workerAppendQString(QString message);
  void workerSetText(QString text);
  void showStopButton();


  private:
  MainWindow *mp_mainWindow = nullptr;
  QElapsedTimer m_time;
  int m_timerDuration;
  SubMonitor *mp_subMonitor = nullptr;

  std::size_t m_count = 0;
  QElapsedTimer m_timerCount;
  QElapsedTimer m_timerStop;

  QString m_bufferMessage;
};

class SubMonitor : public pappso::UiMonitorInterface
{
  friend class WorkMonitor;

  public:
  virtual bool shouldIstop() override;

  virtual void setTotalSteps(std::size_t total_number_of_steps) override;
  virtual void count() override;
  virtual void setTitle(const QString &title) override;
  virtual void setStatus(const QString &status) override;
  virtual void appendText(const QString &text) override;

  protected:
  SubMonitor(WorkMonitor *main_monitor);

  private:
  WorkMonitor *mp_workMonitor;
  QElapsedTimer m_timerStatus;
  int m_timerDuration;
  std::size_t m_count = 0;
};
