/**
 * \file utils/msrunstatisticshandler.h
 * \date 12/08/2018
 * \author Olivier Langella
 * \brief handler on MZ data file to read all spectrums and make basic
 * statistics
 */


/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

#pragma once

#include <pappsomspp/msrun/msrunreader.h>


class MsRunStatisticsHandler : public pappso::SpectrumCollectionHandlerInterface
{
  public:
  virtual ~MsRunStatisticsHandler();
  virtual void setQualifiedMassSpectrum(
    const pappso::QualifiedMassSpectrum &qspectrum) override;
  virtual bool needPeakList() const override;

  unsigned long getMsLevelCount(unsigned int ms_level) const;
  pappso::pappso_double getMsLevelTic(unsigned int ms_level) const;


  unsigned long getTotalCount() const;

  private:
  std::vector<unsigned long> _count_ms_level_spectrum;
  std::vector<pappso::pappso_double> _tic_ms_level_spectrum;
};
