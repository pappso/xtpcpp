/**
 * \file utils/ptmbuilder.cpp
 * \date 16/06/2020
 * \author Olivier Langella
 * \brief manage PTM modes
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#include "ptmbuilder.h"
#include "../grouping/ptm/ptmphospho.h"
#include "../grouping/ptm/ptmacetylation.h"

PtmBuilder::PtmBuilder()
{
}

PtmBuilder::~PtmBuilder()
{
}

PtmInterface *
PtmBuilder::newPtm(PtmMode mode)
{
  PtmInterface *p_ptm = nullptr;
  switch(mode)
    {
      case PtmMode::none:
        break;
      case PtmMode::phospho:
        p_ptm = new PtmPhospho();
        break;
      case PtmMode::acetylation:
        p_ptm = new PtmAcetylation();
        break;
      default:
        break;
    }
  return p_ptm;
}
