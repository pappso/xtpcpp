/**
 * \file utils/fastafiletore.cpp
 * \date 22/4/2017
 * \author Olivier Langella
 * \brief store unique version of fastafile
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "fastafilestore.h"
#include <pappsomspp/utils.h>
#include <QDebug>
FastaFileStore::FastaFileStore()
{
}

FastaFileStore::~FastaFileStore()
{
}

FastaFileSp
FastaFileStore::getInstance(const FastaFile &location)
{
  qDebug() << "FastaFileStore::getInstance() begin ";
  qDebug() << "FastaFileStore::getInstance() begin "
           << location.getAbsoluteFilePath();
  std::vector<FastaFileSp>::iterator it    = _map_fastafile.begin();
  std::vector<FastaFileSp>::iterator itend = _map_fastafile.end();
  while(it != itend)
    {
      if(it->get()->getFilename() == location.getFilename())
        {

          qDebug() << "FastaFileStore::getInstance() end b "
                   << it->get()->getFilename();
          return *it;
        }
      it++;
    }
  FastaFileSp fastafile_sp = std::make_shared<FastaFile>(location);

  fastafile_sp.get()->setXmlId(QString("fasta%1").arg(
    pappso::Utils::getLexicalOrderedString(_map_fastafile.size())));
  _map_fastafile.push_back(fastafile_sp);
  qDebug() << "FastaFileStore::getFastaFileList() end a "
           << _map_fastafile.size();
  return fastafile_sp;
}


const std::vector<FastaFileSp> &
FastaFileStore::getFastaFileList() const
{

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << _map_fastafile.size();
  return _map_fastafile;
}
