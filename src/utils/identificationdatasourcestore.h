/**
 * \file utils/identificationdatasourcestore.h
 * \date 5/4/2017
 * \author Olivier Langella
 * \brief store unique version of identification data sources (output files from
 * identification engines)
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <map>
#include <set>
#include "../core/identification_sources/identificationdatasource.h"
#include "../files/fastafile.h"

class IdentificationDataSourceStore
{
  public:
  IdentificationDataSourceStore();
  ~IdentificationDataSourceStore();
  IdentificationDataSourceSp getInstance(const QString &location);
  IdentificationDataSourceSp getInstance(const QString &location,
                                         IdentificationEngine engine);

  std::vector<IdentificationDataSourceSp>
  getIdentificationDataSourceList() const;

  private:
  std::map<QString, IdentificationDataSourceSp>
    _map_identification_data_sources;
};
