
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once

#include <cstdint>
/*********** enumerations *********************************/

/** \def ExternalDatabase external database references
 *
 */
enum class ExternalDatabase : std::int8_t
{
  OboPsiMod     = 1, ///< OBO PSI MOD
  SwissProt     = 2, ///< Swiss-Prot
  TrEMBL        = 3, ///< TrEMBL
  AGI_LocusCode = 4, ///< AGI_LocusCode
  NCBI_gi       = 5, ///< NCBI_gi
  ref           = 6  ///< ref
};

/** \def IdentificationEngine identification engine
 *
 */
enum class IdentificationEngine : std::int8_t
{
  unknown  = 0, ///< X!Tandem
  XTandem  = 1, ///< MS:1001476 X!Tandem was used to analyze the spectra.
  mascot   = 2, ///< MS:1001207 The name of the Mascot search engine.
  peptider = 3, ///< peptider
  OMSSA = 4, ///< MS:1001475 Open Mass Spectrometry Search Algorithm was used to
             ///< analyze the spectra.
  SEQUEST = 5, ///< MS:1001208 The name of the SEQUEST search engine.
  Comet = 6, ///< MS:1002251 Comet open-source sequence search engine developed
             ///< at the University of Washington. PMID:23148064
  Morpheus = 7, ///< MS:1002661 "Morpheus search engine." [PMID:23323968]
  MSGFplus =
    8, ///< MS:1002048 "MS-GF+ software used to analyze the spectra." [PSI:PI]
  DeepProt = 9, ///< SpecOMS C++ implementation
};

/** \def PeptideEvidenceParam peptide evidence specific parameters
 * is_a: MS:1001143 ! PSM-level search engine specific statistic
 */
enum class PeptideEvidenceParam : std::int8_t
{
  tandem_hyperscore        = 0, ///< X!Tandem hyperscore MS:1001331
  tandem_expectation_value = 1, ///< X!Tandem expectation value MS:1001330
  mascot_score             = 2, ///< PSI-MS MS:1001171 mascot:score 56.16
  mascot_expectation_value =
    3, ///< PSI-MS MS:1001172 mascot:expectation value 2.42102904673618e-006
  peptide_prophet_probability       = 4, ///< no PSI MS description
  peptide_inter_prophet_probability = 5, ///< no PSI MS description
  omssa_evalue        = 6,  ///< MS:1001328  "OMSSA E-value." [PSI:PI]
  omssa_pvalue        = 7,  ///< MS:1001329  "OMSSA p-value." [PSI:PI]
  msgfplus_raw        = 8,  ///< MS:1002049  "MS-GF raw score." [PSI:PI]
  msgfplus_denovo     = 9,  ///< MS:1002050  "MS-GF de novo score." [PSI:PI]
  msgfplus_energy     = 10, ///< MS:1002051  "MS-GF energy score." [PSI:PI]
  msgfplus_SpecEValue = 11, ///< MS:1002052  "MS-GF spectral E-value." [PSI:PI]
  msgfplus_EValue     = 12, ///< MS:1002053  "MS-GF E-value." [PSI:PI]
  msgfplus_isotope_error = 13, ///< MS-GF isotope error
  comet_xcorr   = 14, ///< MS:1002252  "The Comet result 'XCorr'." [PSI:PI]
  comet_deltacn = 15, ///< MS:1002253  "The Comet result 'DeltaCn'." [PSI:PI]
  comet_deltacnstar =
    16, ///< MS:1002254  "The Comet result 'DeltaCnStar'." [PSI:PI]
  comet_spscore = 17, ///< MS:1002255  "The Comet result 'SpScore'." [PSI:PI]
  comet_sprank  = 18, ///< MS:1002256  "The Comet result 'SpRank'." [PSI:PI]
  comet_expectation_value =
    19, ///< MS:1002257  "The Comet result 'Expectation value'." [PSI:PI]
  pappso_qvalue = 20, ///< q-value computed with FDR = decoy / (decoy + target)
  deepprot_original_count = 21, ///< number of matched peak before specfit
  deepprot_fitted_count   = 22, ///< number of matched peak after specfit
  deepprot_match_type =
    23, ///< DeepProt match_type enumeration see pappso::DeepProtMatchType
  deepprot_peptide_candidate_status =
    24, ///< DeepProt peptide candidate status enumeration see
        ///< pappso::DeepProtPeptideCandidateStatus
  deepprot_mass_delta      = 25, ///< mass delta found by DeepProt
  deepprot_delta_positions = 26, ///< list of possible mass delta location on
                                 ///< this peptide found by DeepProt
  msgfplus_QValue    = 27,       ///< MS:1002054  "MS-GF QValue." [PSI:PI]
  msgfplus_PepQValue = 28,       ///< MS:1002055  "MS-GF:PepQValue" [PSI:PI]

};

/** \def IdentificationEngineParam identification engine parameters
 *
 */
enum class IdentificationEngineParam : std::int8_t
{
  tandem_param = 0 ///< X!Tandem xml parameters file
};


/** \def IdentificationEngineStatistics identification engine statistics
 *
 */
enum class IdentificationEngineStatistics : std::int8_t
{
  total_spectra_assigned =
    1, ///< total_spectra_assigned in one identification file (one sample)
  total_spectra_used =
    2, ///< total_spectra_used in one identification file (one sample)
  total_peptide_used =
    3, ///< total number of peptides generated and used in identification
  total_proteins_used =
    4, ///< total number of proteins generated and used in identification
  total_unique_assigned = 5, ///< total number unique peptide sequence assigned
};

/** \def MsRunStatistics MS run statistics
 *
 */
enum class MsRunStatistics : std::int8_t
{
  total_spectra     = 1, ///< total number of spectra
  total_spectra_ms1 = 2, ///< total number of MS1 spectra
  total_spectra_ms2 = 3, ///< total number of MS2 spectra
  total_spectra_ms3 = 4, ///< total number of MS3 spectra
  tic_spectra_ms1   = 5, ///< total ion current in MS1 spectra
  tic_spectra_ms2   = 6, ///< total ion current in MS2 spectra
  tic_spectra_ms3   = 7, ///< total ion current in MS3 spectra
};


/** \def ProjectMode separate each samples or combine all
 *
 */
enum class ProjectMode : std::int8_t
{
  individual = 1, ///< separate each biological samples (2D spots for example)
  combined   = 2  ///< combine every MS runs to get only one protein list
};


/** \def GroupingType list of available grouping algoritms
 *
 */
enum class GroupingType
{
  PeptideMass, ///< protein grouper algo
  Phospho,     ///< phospho peptides grouping
  SampleScan   ///< i2MassChroQ algo
};


/** \def ValidationState
 *
 */

enum class ValidationState : std::int8_t
{
  notValid        = 0, ///< notValid : automatic filter validation failed
  valid           = 1, ///< valid : automatic filter validation passed
  validAndChecked = 2, ///< validAndChecked : automatic filter validation passed
                       ///< + manual checking
  grouped = 3 ///< grouped : automatic filter validation passed + manual
              ///< checking + grouped
};


/** \def export fasta files
 *
 */

enum class ExportFastaType : std::int8_t
{
  all           = 0, ///< all grouped proteins
  oneBySubgroup = 1, ///< export only one by subgroup
  oneByGroup    = 2, ///< export only one by group
};


/** \def TableFileFormat file format of tables
 *
 */
enum class TableFileFormat : std::int8_t
{
  ods = 1, ///< Open Document Spreadsheet
  tsv = 2, ///< tabulated separated values
};


/** \def ContaminantRemovalMode defines how to remove contaminants from grouping
 *
 */
enum class ContaminantRemovalMode : std::int8_t
{
  before = 1, ///< contaminants are ignored before grouping
  peptides =
    2, ///< any protein containing one or more contaminant peptide is removed
  groups =
    3, ///< any group containing one or more contaminant protein is removed
};


/** \def PtmMode defines what kind of PTM is used to visualize PTM islands
 *
 */
enum class PtmMode : std::int8_t
{
  none        = 0, ///< PTM is not relevant for this project
  phospho     = 1, ///< use Phosphorylation for PTM
  acetylation = 2, ///< use Acetylation Nter and Lysine MOD:00394 for PTM
};


/** \def McqrLoadDataMode defines the different ways to load raw data in MCQR
 *
 */
enum class McqrLoadDataMode : std::int8_t
{
  basic    = 0, ///< mcq.read.masschroq
  fraction = 1, ///< mcq.read.masschroq.fraction
  label    = 2, ///< mcq.read.masschroq.label
  both     = 3, ///<
};


/** \def McqrExperimentType spectral count, xic
 *
 */
enum class McqrExperimentType : std::int8_t
{
  spectralCount = 0, ///< use i2masschroq data
  xic           = 1, ///< use masschroq results
};


/** \def CondorJobStatus HTCondor job status
 * */
enum class CondorJobStatus : std::int8_t
{
  Unexpanded     = 0, ///< 0 Unexpanded U
  Idle           = 1, ///< 1 Idle I
  Running        = 2, ///< 2 Running R
  Removed        = 3, ///< 3 Removed X
  Completed      = 4, ///< 4 Completed C
  Held           = 5, ///< 5 Held H
  Submission_err = 6  ///< 6 Submission_err E
};
