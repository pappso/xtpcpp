
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/
#include "groupingexperiment.h"

#pragma once

class GrpGroupingMonitor;

class GroupingPeptideMass : public GroupingExperiment
{
  friend class GroupingExperiment;

  public:
  virtual ~GroupingPeptideMass();

  virtual pappso::GrpProteinSp &
  getGrpProteinSp(ProteinMatch *p_protein_match) override;
  virtual pappso::GrpPeptideSp &
  setGrpPeptide(pappso::GrpProteinSp proteinSp,
                PeptideEvidence *p_peptide_evidence) override;
  virtual void
  addPostGroupingGrpProteinSpRemoval(pappso::GrpProteinSp sp_protein) override;

  virtual void startGrouping() override;

  protected:
  GroupingPeptideMass(ContaminantRemovalMode contaminantRemovalMode,
                      WorkMonitorInterface *p_work_monitor);

  private:
  GrpGroupingMonitor *_p_monitor = nullptr;
  pappso::GrpExperiment *_p_grp_experiment;
};
