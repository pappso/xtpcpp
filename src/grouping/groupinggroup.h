
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef GROUPINGGROUP_H
#define GROUPINGGROUP_H

#include <memory>
#include <vector>
#include <QStringList>
#include "../utils/types.h"
#include "../core/msrun.h"
#include "../core/labeling/label.h"

class ProteinMatch;
class PeptideEvidence;

class GroupingGroup;
typedef std::shared_ptr<GroupingGroup> GroupingGroupSp;

class GroupingGroup
{
  public:
  GroupingGroup();
  ~GroupingGroup();

  unsigned int getGroupNumber() const;

  void add(const ProteinMatch *p_protein_match);
  std::size_t countSpecificSampleScan(const ProteinMatch *p_protein_match,
                                      ValidationState state,
                                      const MsRun *p_msrun_id = nullptr,
                                      const Label *p_label    = nullptr) const;
  std::size_t countSpecificSequenceLi(const ProteinMatch *p_protein_match,
                                      ValidationState state,
                                      const MsRun *p_msrun_id = nullptr,
                                      const Label *p_label    = nullptr) const;

  unsigned int getNumberOfSubgroups() const;

  /** @brief count proteins in subgroup
   * */
  unsigned int countProteinInSubgroup(unsigned int subgroup_number) const;

  /** @brief count number of subgroups in which this peptide is present
   * */
  unsigned int
  countSubgroupPresence(const PeptideEvidence *p_peptide_evidence) const;

  /** @brief subgroup identifier list in which this peptide is present
   * */
  const QStringList
  getSubgroupIdList(const PeptideEvidence *p_peptide_evidence) const;

  /** @brief give the protein group id of the representant of a subgroup
   * */
  QString getProteinGroupingIdOfSubgroup(unsigned int subgroup_number) const;


  /** @brief get the list of protein match included in this group
   * */
  const std::vector<const ProteinMatch *> &getProteinMatchList() const;

  /** @brief get the list of peptide evidence included in this group
   * warning : it only contains the first protein representant in one subgroup
   * use a double loop with getProteinMatchList to scan every peptide match
   * */
  std::vector<const PeptideEvidence *> getPeptideEvidenceList() const;

  const std::vector<std::pair<unsigned int, const PeptideEvidence *>> &
  getPairSgNumberPeptideEvidenceList() const;

  private:
  unsigned int _group_number       = 0;
  unsigned int _number_of_subgroup = 0;
  std::vector<std::pair<unsigned int, const PeptideEvidence *>>
    _pair_sg_number_peptide_evidence_list;

  std::vector<const ProteinMatch *> _protein_match_list;
};

#endif // GROUPINGGROUP_H
