/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QXmlDefaultHandler>
#include <pappsomspp/pappsoexception.h>
#include "../core/proteinxtp.h"
#include "../core/peptidextp.h"
#include <pappsomspp/amino_acid/aamodification.h>
#include "../core/project.h"
#include "../core/proteinmatch.h"
#include "../utils/workmonitor.h"

class XpipSaxHandler : public QXmlDefaultHandler
{
  public:
  XpipSaxHandler(pappso::UiMonitorInterface *p_monitor, Project *p_project);
  ~XpipSaxHandler();

  bool startElement(const QString &namespaceURI,
                    const QString &localName,
                    const QString &qName,
                    const QXmlAttributes &attributes);

  bool endElement(const QString &namespaceURI,
                  const QString &localName,
                  const QString &qName);

  bool startDocument();

  bool endDocument();

  bool characters(const QString &str);

  bool fatalError(const QXmlParseException &exception);
  bool error(const QXmlParseException &exception);

  QString errorString() const;
  bool isJavaXpip() const;


  private:
  bool startElement_filter_params(QXmlAttributes attributes);
  bool startElement_information(QXmlAttributes attributes);
  bool startElement_identification(QXmlAttributes attributes);
  bool startElement_match(QXmlAttributes attributes);
  bool startElement_peptide(QXmlAttributes attributes);
  bool startElement_protein(QXmlAttributes attributes);
  bool startElement_sample(QXmlAttributes attributes);
  bool startElement_modifs_mass(QXmlAttributes attributes);
  bool startElement_modif(QXmlAttributes attributes);
  bool endElement_identification();
  bool endElement_sequence();
  bool endElement_protein();
  bool endElement_peptide();
  bool endElement_match();

  pappso::AaModificationP getAaModificationP(pappso::pappso_double mass) const;

  private:
  pappso::UiMonitorInterface *_p_monitor;
  std::vector<QString> _tag_stack;
  QString _errorStr;
  QString _current_text;
  AutomaticFilterParameters _automatic_filter_parameters;

  Project *_p_project;
  ProteinMatch *_p_protein_match;
  PeptideEvidence *_p_peptide_evidence;
  PeptideMatch _current_peptide_match;
  ProteinXtp _current_protein;
  PeptideXtpSp _current_peptide_sp;
  IdentificationGroup *_current_identification_group_p;
  FastaFileSp _current_fasta_file_sp;

  QMap<QString, pappso::AaModificationP> _map_massstr_aamod;
  std::size_t _count_protein_match = 0;
  std::size_t _total_protein_match;

  bool _is_java_xpip = false;
};
