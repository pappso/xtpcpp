/**
 * \file input/metadataodshandler.h
 * \date 13/01/2021
 * \author Thomas Renne
 * \brief set MCQR params and run
 */

/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <odsstream/odsdocreader.h>
#include "../core/project.h"

class MetadataOdsHandler : public OdsDocHandlerInterface
{
  public:
  MetadataOdsHandler(ProjectSp project);
  ~MetadataOdsHandler();

  virtual void startSheet([[maybe_unused]] const QString &sheet_name) override;
  virtual void endSheet() override;
  virtual void startLine() override;
  virtual void endLine() override;
  virtual void setCell(const OdsCell &cell) override;
  virtual void endDocument() override;

  private:
  void setColumnVector(const OdsCell &cell);

  private:
  int m_rowNumber;
  int m_colNumber;
  int m_sheetNumber = 0;
  ProjectSp msp_project;
  McqrMetadata m_mcqrMetadataLine;
  QStringList m_cols;
};
