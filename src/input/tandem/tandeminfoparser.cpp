/**
 * \file src/input/tandem/tandeminfoparser.cpp
 * \date 23/12/2021
 * \author Olivier Langella
 * \brief reads tandem xml result files to parse various informations
 */

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "tandeminfoparser.h"
#include <QDebug>
#include <QFileInfo>
#include <QDir>

TandemInfoParser::TandemInfoParser()
{
}

TandemInfoParser::TandemInfoParser(const TandemInfoParser &other)
{
  m_spectrum_path = other.m_spectrum_path;
  m_modelCount    = other.m_modelCount;
  m_mzFormat      = other.m_mzFormat;
}

TandemInfoParser::~TandemInfoParser()
{
}

void
TandemInfoParser::readStream()
{
  qDebug();
  m_modelCount = 0;
  if(m_qxmlStreamReader.readNextStartElement())
    {
      if(m_qxmlStreamReader.name() == "bioml")
        {
          qDebug();
          while(m_qxmlStreamReader.readNextStartElement())
            {
              qDebug() << m_qxmlStreamReader.name();

              if(m_qxmlStreamReader.name() == "group")
                {
                  // read_note();
                  QString type =
                    m_qxmlStreamReader.attributes().value("type").toString();
                  QString label =
                    m_qxmlStreamReader.attributes().value("label").toString();


                  qDebug() << m_qxmlStreamReader.name() << " type=" << type
                           << " label=" << label;

                  if(type == "model")
                    {
                      m_modelCount++;
                      m_qxmlStreamReader.skipCurrentElement();
                    }
                  else if(type == "parameters")
                    {

                      while(m_qxmlStreamReader.readNextStartElement())
                        {
                          if(m_qxmlStreamReader.name() == "note")
                            {

                              QString type = m_qxmlStreamReader.attributes()
                                               .value("type")
                                               .toString();
                              QString label = m_qxmlStreamReader.attributes()
                                                .value("label")
                                                .toString();

                              qDebug()
                                << m_qxmlStreamReader.name() << " type=" << type
                                << " label=" << label;

                              if((type == "input") &&
                                 (label == "spectrum, path"))
                                {
                                  //<note type="input" label="spectrum, path">

                                  m_spectrum_path =
                                    m_qxmlStreamReader.readElementText();


                                  QFileInfo fileinfo(m_spectrum_path);
                                  if(fileinfo.fileName() == "analysis.tdf")
                                    {
                                      m_spectrum_path =
                                        fileinfo.absoluteDir().absolutePath();
                                    }
                                  // m_qxmlStreamReader.skipCurrentElement();
                                }

                              else if((type == "input") &&
                                      (label == "spectrum, mzFormat"))
                                {

                                  m_mzFormat =
                                    (pappso::MzFormat)m_qxmlStreamReader
                                      .readElementText()
                                      .toInt();
                                }
                              else if((type == "input") &&
                                      (label == "output, path"))
                                {
                                  //<note type="input" label="output, path">
                                  // m_qxmlStreamReader.skipCurrentElement();

                                  m_qxmlStreamReader.skipCurrentElement();
                                }
                              // list path, default parameters
                              else if((type == "input") &&
                                      (label ==
                                       "list path, default parameters"))
                                {
                                  //<note type="input" label="list path, default
                                  // parameters">/gorgone/pappso/tmp/i2masschroq.AjyZGg/Lumos_trypsin_rev_camC_oxM_10ppm_HCDOT_12102017CH.xml</note>

                                  m_qxmlStreamReader.skipCurrentElement();
                                }
                              else
                                {
                                  m_qxmlStreamReader.skipCurrentElement();
                                }
                            }
                          else
                            { // note
                              m_qxmlStreamReader.raiseError(QObject::tr(
                                "Not an X!Tandem input file (no note)"));
                              m_qxmlStreamReader.skipCurrentElement();
                            }
                        }
                    }
                }
              else
                { // group
                  m_qxmlStreamReader.raiseError(
                    QObject::tr("Not an X!Tandem input file (no group)"));
                  m_qxmlStreamReader.skipCurrentElement();
                }
            }
        }
      else
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("Not an X!Tandem input file (no bioml)"));
          m_qxmlStreamReader.skipCurrentElement();
        }
    }

  qDebug();
}

const QString &
TandemInfoParser::getSpectraDataLocation() const
{
  return m_spectrum_path;
}

std::size_t
TandemInfoParser::getModelCount() const
{
  return m_modelCount;
}

pappso::MzFormat
TandemInfoParser::getMzFormat() const
{
  return m_mzFormat;
}
