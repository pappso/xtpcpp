/**
 * \file src/input/mzidentml/msrunsaxhandler.h
 * \date 23/02/2021
 * \author Olivier Langella
 * \brief parse mzIdentML result file to only get the msrun
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#pragma once

#include <QXmlDefaultHandler>
#include <pappsomspp/pappsoexception.h>

class MsRunSaxHandler : public QXmlDefaultHandler
{
  public:
  MsRunSaxHandler();
  ~MsRunSaxHandler();

  const QString &getSpectraDataLocation() const;

  bool startElement(const QString &namespaceURI,
                    const QString &localName,
                    const QString &qName,
                    const QXmlAttributes &attributes);

  bool endElement(const QString &namespaceURI,
                  const QString &localName,
                  const QString &qName);

  bool startDocument();

  bool endDocument();


  bool fatalError(const QXmlParseException &exception);
  bool error(const QXmlParseException &exception);

  QString errorString() const;

  private:
  bool startElement_SpectraData(QXmlAttributes attributes);


  bool endElement_SpectraData();


  private:
  std::vector<QString> m_tagStack;
  QString m_errorStr;

  QString m_spectraDataLocation = "";
};
