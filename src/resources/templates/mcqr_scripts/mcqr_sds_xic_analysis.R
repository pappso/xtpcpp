{% if mcqr_step == 0 %} # SdsXicAnalysisStep::quality_xic
  message("MCQRBegin: quality_xic")
  cat("<h1>MCQR SDS XIC Analysis</h1>")

  setwd("{{ rdata_folder }}")
  capture.output(library("MCQR", verbose=FALSE), file=stderr())
  message(paste("MCQRInfo: MCQR version :", packageVersion("MCQR")))
  library("xtable")
  library("svglite")
  library("readODS")

  #**************************************************************************************************************************************************
  #**************************************************************************************************************************************************
  #******************************************************   3. Analyzing quantification data obtained from extracted ion chromatogram (XIC)
  #**************************************************************************************************************************************************
  #**************************************************************************************************************************************************

  cat("<h2>Analyzing quantification data obtained from extracted ion chromatogram (XIC)</h2>")
  #********************************************************
  #--------------------------------------------------------
  #             3.1 Data and metadata loading
  #--------------------------------------------------------
  #********************************************************
  cat("<h3>Overview of the XIC loaded</h3>")
  # Loading MassChroq data. NB: peptide intensities measured as peak areas are automatically log10-transformed.
  # Directory names must not include a trailing backslash or slash on Windows
  capture.output(XICRAW.SDS <- mcq.read.masschroq.fraction(directory="{{xic_directory}}"), file=stderr())

  load("{{ rdata_path }}")
  write_ods(metadata.data, "metadata.ods")

  # Importing the filled in metadata file
  capture.output(META <- mcq.read.metadata("metadata.ods"), file=stderr()) 
  # Attaching metadata to quantification data
	XICRAW.SDS <- mcq.merge.metadata(XICRAW.SDS, metadata=META)

  # Overview of the content of the 'XICRAW' object
  summary (XICRAW.SDS)

	#********************************************************
  #--------------------------------------------------------
  #             3.2. Checking data quality
  #--------------------------------------------------------
  #********************************************************


  #****************************************************************************************************************
  # 3.2.1. Checking chromatography
  #****************************************************************************************************************
  cat("<h3>Chromatography checking</h3>")
  cat("<h4>Plot distribution of the width of chromatographic peaks</h4>")
  # Checking the distribution of the width of chromatographic peaks. Are there many very large peaks?
  svglite("{{ tmp_path }}/peak_width.svg", width=14, height=12)
  capture.output(mcq.plot.peak.width(XICRAW.SDS, byFraction = FALSE), file=stderr())
  capture.output(dev.off(), file=stderr())
  cat("<p><img src=\"{{ tmp_path }}/peak_width.svg\" /></p>")
  
  cat("<h4>Plot distribution of the standard deviation of RT</h4>")
  # Checking the variability of the peptide-mz retention time (RT) by plotting the distribution of the standard deviation of RT. Are there many peptides-mz showing huge variations of their RT?
  svglite("{{ tmp_path }}/rt_variability.svg", width=14, height=12)
  capture.output(mcq.plot.rt.variability(XICRAW.SDS, limit={{ upper_x_axis }}), file=stderr())
  capture.output(dev.off(), file=stderr())
  cat("<p><img src=\"{{ tmp_path }}/rt_variability.svg\" /></p>")

  #****************************************************************************************************************
  # 3.2.2. Checking injections
  #****************************************************************************************************************

  cat("<h3>Plot injections checking</h3>")
  cat("<h4>Plot number of peptides by tracks and fraction</h4>")
  # Checking the number of chromatographic peaks quantified in each injection. Are there injections with much fewer peaks?
  svglite("{{ tmp_path }}/injection_counts.svg", width=14, height=12)
  capture.output(mcq.plot.counts(XICRAW.SDS, overlay=TRUE), file=stderr())
  capture.output(dev.off(), file=stderr())
  cat("<p><img src=\"{{ tmp_path }}/injection_counts.svg\" /></p>")
  
  cat("<h4>Plot intensity profiles for selected factors</h4>")
  #  Checking the median intensity along the chromatography. Are there injections showing abnormal intensity profiles?
  svglite("{{ tmp_path }}/intensity_profile.svg", width=14, height=12)
  capture.output(mcq.plot.intensity.profile(XICRAW.SDS, factorToColor=c({{ factors_color }}), RTinterval={{ rt_interval }}), file=stderr())
  capture.output(dev.off(), file=stderr())
  cat("<p><img src=\"{{ tmp_path }}/intensity_profile.svg\" /></p>")
  
  cat("<h4>Plot distribution of peptide intensities for each injection</h4>")
  # Checking the distribution of the peptide-mz log-intensities in each injection. Are there injections where intensities are not normally distributed?
  svglite("{{ tmp_path }}/intensity_distribution%01d.svg", width=14, height=12)
  capture.output(mcq.plot.intensity.distribution(XICRAW.SDS), file=stderr())
  capture.output(dev.off(), file=stderr())
  
  nb_msrun = dim(META@metadata)[1]
  for(i in 1:nb_msrun){
    cat(paste0("<p><img src=\"{{ tmp_path }}/intensity_distribution", i, ".svg\" /></p>"))
  }

  cat("<h4>Plot number of fractions per peptide</h4>")
  # Checking the number of fractions per peptide-mz. 
  svglite("{{ tmp_path }}/fractions_peptide.svg", width=14, height=12)
  mcq.plot.fractions.per.peptide(XICRAW.SDS)
  capture.output(dev.off(), file=stderr())
  cat("<p><img src=\"{{ tmp_path }}/fractions_peptide.svg\" /></p>")
  
  {% if sds_page %}
    cat("<h3>SDS-PAGE representation</h3>")
    # Graphical representation of the number of peptide-mz per fraction and track for each protein. 
    # Warning this function is very long 
    # Warining affichage bug pour l'instant 
    # Display of the graph on the screen
    svglite("{{ tmp_path }}/sds_page_%01d.svg", width=14, height=12)
    mcq.plot.sds.page(XICRAW.SDS)
    capture.output(dev.off(), file=stderr())
    cat("<p><img src=\"{{ tmp_path }}/sds_page_1.svg\" /></p>")
  {% endif %}

  message("MCQREnd: quality_xic")

{% elif mcqr_step == 1%} #SdsXicAnalysisStep::filtering_data
  message("MCQRBegin: filtering_data")
  cat("<h2>Analyzing intensity data (quantitative variations)</h2>")
  #********************************************************
  #--------------------------------------------------------
  #       3.3. Analyzing intensity data (quantitative variations)
  #--------------------------------------------------------
  #********************************************************

  #****************************************************************************************************************
  # 3.3.1. Filtering dubious data
  #****************************************************************************************************************

  #############  If necessary, removing dubious fractions (The proteins list of the fractions eliminated is exported in the file protlist_from_removed_fractions.tsv)
  {% if levels_drop|length > 0 %}
    XIC.SDS_clean <- mcq.drop(XICRAW.SDS, factor="{{ factor_drop }}", levels=c({{ levels_drop }}))
  {% else %}
    XIC.SDS_clean <- XICRAW.SDS
  {% endif %}
  # The difference between the list of proteins of XIRAW.SDS and the list of proteins of XIC.SDS gives the list of protein that are uniquely in the removed fractions. 
  capture.output(XICRAW.SDS.prot <- mcq.get.protlist(XICRAW.SDS), file=stderr())
  capture.output(XIC.SDS.prot <- mcq.get.protlist(XIC.SDS_clean), file=stderr())
  capture.output(protlist_only_removed_fractions <- setdiff(XICRAW.SDS.prot,XIC.SDS.prot), file=stderr())

  #############  If necessary, removing dubious chromatographic data
  # Removing peptides-mz showing too much variations of their retention time. These peptides-mz may occur from mis-identifications. Use the plot produced by the mcq.plot.rt.variability function to decide on the cut-off of standard deviation to use (e.g. here, 20 seconds).
  capture.output(XIC.SDS <- mcq.drop.variable.rt(XIC.SDS_clean, cutoff={{ cutoff_rt }}, byFraction = {{ fraction_rt }}), file=stderr())
    
  # Removing peptides-mz associated to very large chromatographic peaks. Use the plot produced by the mcq.plot.peak.width function to decide on the cut-off to use (e.g. here, 200 seconds). Don't be too stringent with this filter because the peak width is proportional to the peak height. A large peak may therefore simply correspond to a peptide-mz that is particularly intense in a given injection. 
  capture.output(XIC.SDS <- mcq.drop.wide.peaks(XIC.SDS, cutoff={{ cutoff_peaks }}, byFraction = {{ fraction_peaks }}), file=stderr())

  ############# Display of a summary of the 'XIC' object after removal of dubious peptides
  cat("<h3>XIC summary after filtering</h3>")
  summary(XIC.SDS)
  message("MCQREnd: filtering_data")

{% elif mcqr_step == 2%}
  message("MCQRBegin: reconstiting_fraction")
  #****************************************************************************************************************
  # 3.3.2. Reconstituting samples from fractions
  #****************************************************************************************************************
  cat("<h3>Reconstituting samples from fractions</h3>")
  cat("<h4>XIC summary after track reconstituted</h4>")
  ############# Computing peptide intensities by track as the sum of the peptide intensities of all fractions for each track
  XIC.BY.TRACK <- mcq.compute.quantity.by.track(XIC.SDS)
  summary(XIC.BY.TRACK)
    
  ############# Checking intensity profiles for the peptide_mz belonging to the same protein
  ### pas posible pour l'instant
  # Display of the graph on the screen for 10 proteins
#   mcq.plot.peptide.intensity(XIC.BY.TRACK, flist=c({{ factor_list }}), rCutoff = {{ r_cutoff }},   showImputed = {{ show_imputed }}, nprot= {{ n_prot }}, log= {{ log }}, scale = {{ scale }})

  #############  Checking the distribution of the peptide intensities in the reconstituted samples (tracks). Are there samples showing odd distribution?
  cat("<h4>Peptide intensities distribution in the reconstituted samples</h4>")
  # Display of the graph on the screen
  svglite("{{ tmp_path }}/intensity_violin.svg", width=14, height=12)
  mcq.plot.intensity.violin(XIC.BY.TRACK, factorToColor=c({{ factor_color }}))
  capture.output(dev.off(), file=stderr())
  cat("<p><img src=\"{{ tmp_path }}/intensity_violin.svg\" /></p>")
  

  ############# Checking the global correlation between the peptide-mz intensities of one sample and the peptide-mz intensities of a sample chosen as reference. Are the correlation globally correct?
  cat("<h4>Plot global correlation between all tracks vs {{ track_level }}</h4>")
  # Display of the graph on the screen
  svglite("{{ tmp_path }}/intensity_correlation_%01d.svg", width=14, height=12)
  capture.output(mcq.plot.intensity.correlation(XIC.BY.TRACK, ref="{{ track_level }}"), file=stderr())
  capture.output(dev.off(), file=stderr())
  
  nb_tracks = length(XIC.BY.TRACK@metadata@metadata[["track"]]) - 1
  nb_pages = nb_tracks%/%4
    if(nb_tracks%%4 != 0) {
      nb_pages = nb_pages + 1
    }
    for(i in 1:nb_pages){
      cat(paste0("<p><img src=\"{{ tmp_path }}/intensity_correlation_", i, ".svg\" /></p>"))
    }
  message("MCQREnd: reconstiting_fraction")
  
{% elif mcqr_step == 3 %}  
  message("MCQRBegin: normalizing_peptides")
  #****************************************************************************************************************
  # 3.3.3. Normalizing peptide-mz intensities to take into account possible global quantitative variations between LC-MS runs
  #****************************************************************************************************************

  cat("<h3>Normalizing peptide-mz intensities</h3>")
  ############# Normalizing peptide-mz intensities
  # Normalization by a median-based method ("median" or "median.RT", based on a reference sample) or by a "percent" method. See help for details
  capture.output(XIC <- mcq.compute.normalization(XIC.BY.TRACK, ref="{{ track_ref }}", method="{{ norm_method }}"), file=stderr())

  cat("<h4>Control the normalization effets</h4>")
  ############# Controling the effect of normalization on intensities distribution
  # Display of the graph of intensity distributions on the screen
  svglite("{{ tmp_path }}/norm_violin.svg", width=14, height=12)
  mcq.plot.intensity.violin(XIC, factorToColor=c({{ factors_color }})) 
 capture.output(dev.off(), file=stderr())
  cat("<p><img src=\"{{ tmp_path }}/norm_violin.svg\" /></p>")
  
  cat("<h4>Principal Component Analysis</h4>")
  # PCA using all the samples as individuals
  capture.output(XIC_PCA <- mcq.compute.pca(XIC, flist=c({{ factors_list }})), file=stderr())
  
  # Display of the PCA on the screen. Possibility to modify the 'factorToColor' and 'labels' arguments to change the colors and the labels of the individuals represented on the PCA.	
  svglite("{{ tmp_path }}/xic_pca_%01d.svg", width=14, height=12)
  capture.output(mcq.plot.pca(XIC_PCA, factorToColor=c({{ factors_color }}), labels = c({{ factors_label }}), tagType="both", labelSize=4), file=stderr())
  capture.output(dev.off(), file=stderr())
  cat("<p><img src=\"{{ tmp_path }}/xic_pca_1.svg\" /></p>")
  cat("<p><img src=\"{{ tmp_path }}/xic_pca_2.svg\" /></p>")
  cat("<p><img src=\"{{ tmp_path }}/xic_pca_3.svg\" /></p>")  
  
  # message("MCQRInfo: PCA done")
  message("MCQREnd: normalizing_peptides")

{% elif mcqr_step == 4 %}
  message("MCQRBegin: filtering_shared")
  cat("<h3>Filtering peptides</h3>")
  #****************************************************************************************************************
  # 3.3.4. Filtering for shared, unreproducible or uncorrelated peptides-mz
  #****************************************************************************************************************

  ############# Checking the distribution of the number of proteins to which a peptide-mz belongs. Remark : a peptide that belongs to only one protein in the dataset is not proteotypic to this protein. It is in fact specific of a sub-group of XTandemPipeline

  cat("<h4>Plot peptide sharing</h4>")
  # Display of the graph on the screen
  svglite("{{ tmp_path }}/peptide_sharing.svg", width=14, height=12)
  mcq.plot.peptide.sharing(XIC) 
  capture.output(dev.off(), file=stderr())
  cat("<p><img src=\"{{ tmp_path }}/peptide_sharing.svg\" /></p>")
  
  ############# Removing shared peptides
  capture.output(XIC <- mcq.drop.shared.peptides(XIC), file=stderr())

  ############# Checking the reproducibility of the peptides-mz
  cat("<h4>Plot reproducibility of peptides</h4>")
  # Display of the graph on the screen
  svglite("{{ tmp_path }}/peptide_reproducible_%01d.svg", width=14, height=12)
  mcq.plot.peptide.reproducibility(XIC)
  capture.output(dev.off(), file=stderr())
  cat("<p><img src=\"{{ tmp_path }}/peptide_reproducible_1.svg\" /></p>")
  cat("<p><img src=\"{{ tmp_path }}/peptide_reproducible_1.svg\" /></p>")

  ############# Removing peptides-mz showing too many missing data (here two conditions "mutant" WT and KO, 4 replicates. With percentNA=0.5, peptides-mz showing 2 or more missing values per "mutant" condition are therefore removed. We tolerate 4 missing values.
  capture.output(XIC <- mcq.drop.unreproducible.peptides(XIC, method="{{ drop_method }}", percentNA={{ percent_na }}), file=stderr())

  # Display of a summary of the 'XIC' object after removal of peptides that are absent in more than a given proportion of the injections
  cat("<h4>XIC summary after filtering shared and unreproducible peptides</h4>")
  summary(XIC)

  ############# Checking of the correlation of the peptides-mz belonging to a same protein

  # Display of the graph on the screen
  cat("<h4>Plot the peptide intensity to choose correlation cutoff</h4>")
  svglite("{{ tmp_path }}/peptide_intensity_corr.svg", width=14, height=12)
  capture.output(PROTS <- mcq.plot.peptide.intensity(XIC, flist=c({{ factors_list }}), rCutoff = {{ corr_cutoff }},  nprot={{ n_prot_corr }}, getProtlist=TRUE, showImputed={{ filter_imputed }}, log = {{ filter_log }}), file=stderr())
  capture.output(dev.off(), file=stderr())
  cat("<p><img src=\"{{ tmp_path }}/peptide_intensity_corr.svg\" /></p>")
  
  message("MCQREnd: filtering_shared")
  
{% elif mcqr_step == 5 %}
  message("MCQRBegin: filtering_uncorrelated")
  ############# Removing the peptides-mz whose intensity profile deviates from the average profile of the peptides-mz from the same protein. This filter must be applied only if the number of levels within one of the factors of interest (factors like replicates not included) is superior or equal to 5 because it is based on the correlation between peptides-mz belonging to the same protein.  
  capture.output(XIC <- mcq.drop.uncorrelated.peptides(XIC, flist=c({{ factors_list }}), rCutoff = {{ cor_cutoff }}), file=stderr())


  ############# Removing proteins quantified by a small number of peptides-mz. When the minimal number of peptides-mz required is 2, this filter is automatically included in the mcq.drop.uncorrelated.peptides function. The function mcq.drop.proteins.with.few.peptides is therefore unnecessary.
  capture.output(XIC <- mcq.drop.proteins.with.few.peptides(XIC, npep = {{ nb_pep }}), file=stderr())

  # Display of a summary of the 'XIC' object after removal of uncorrelated peptides
  cat("<h4>XIC summary after filtering uncorrelated peptides</h4>")
  summary(XIC)

  message("MCQREnd: filtering_uncorrelated")
  
{% elif mcqr_step == 6 %}
  message("MCQRBegin: imputing")
  cat("<h3>Imputing missing peptide intensities and protein abundance</h3>")
  #****************************************************************************************************************
  # 3.3.5. Imputing missing peptide-mz intensities
  #****************************************************************************************************************

  # Imputing missing peptide-mz intensities. No imputation is made for peptide-mz intensity when a protein is completely absent from a sample.
  capture.output(XIC <- mcq.compute.peptide.imputation(XIC, method="{{ imputation_method }}"), file=stderr())

  # Display of a summary of the 'XIC' object after peptides imputation
  cat("<h4>XIC summary after imputing missing peptide intensities</h4>")
  summary(XIC)

  #****************************************************************************************************************
  # 3.3.6. Computing protein abundances
  #****************************************************************************************************************

  ############# Computing protein abundances
  # Computation of the protein abundances as the sum of the peptide intensities
  capture.output(XICAB <- mcq.compute.protein.abundance(XIC, method="{{ abundance_method }}"), file=stderr())

  # Display of a summary of the 'XICAB' object  
  cat("<h4>XIC summary after computing protein abundance</h4>")
  summary(XICAB)

  #****************************************************************************************************************
  # 3.3.7. Imputing missing protein abundances
  #****************************************************************************************************************
  
  ############# Imputing missing protein abundances by the minimum abundance obtained for each protein in the whole experiment
  capture.output(XICAB <- mcq.compute.protein.imputation(XICAB), file=stderr())

  # Display of a summary of the 'XICAB' object  after proteins imputation
  cat("<h4>XIC summary after imputing protein abundance</h4>")
  summary(XICAB)
 
  #****************************************************************************************************************
  # 3.3.8. Overview of the protein abundance data
  #****************************************************************************************************************
  cat("<h2>Overview of the protein abundance data</h2>")

  #############  PCA representation
  cat("<h3>Principal Component Analysis</h3>")
  # PCA using a factor or a combination of factors as individuals (in this case, average abundances are computed for the factor or for the combination of factors)
  capture.output(XICAB_PCA <- mcq.compute.pca(XICAB, flist=c({{ factors_list }})), file=stderr())
    
  # Display of the PCA on the screen. Possibility to modify the 'factorToColor' and 'labels' arguments to change the colors and the labels of the individuals represented on the PCA.	
  svglite("{{ tmp_path }}/xic_ab_pca_%01d.svg", width=14, height=12)
  capture.output(mcq.plot.pca(XICAB_PCA, factorToColor=c({{ factors_color }}), labels = c({{ factors_label }}), tagType="both", labelSize=4), file=stderr())
  capture.output(dev.off(), file=stderr())
  cat("<p><img src=\"{{ tmp_path }}/xic_ab_pca_1.svg\" /></p>")
  cat("<p><img src=\"{{ tmp_path }}/xic_ab_pca_2.svg\" /></p>")
  cat("<p><img src=\"{{ tmp_path }}/xic_ab_pca_3.svg\" /></p>")  

  message("MCQREnd: imputing")

{% elif mcqr_step == 7%}  

  message("MCQRBegin: heatmap")
  ############# Plotting a heatmap
  # Exporting the graph in a .pdf file in '/path/to/my_working_directory' and the protein list to write
  cat("<h4>Heatmap of the protein abundance</h4>")
  # Display of the graph on the screen. Possibility to change the color, the method of aggregation ('method' argument) and the method to compute the distances ('distfun' argument). See help for details
  png("{{ tmp_path }}/prot_heatmap.png", width=1000, height=1000)
  heatmap.prot.list <- mcq.plot.heatmap(XICAB, flist=c({{ factors_list }}), factorToColor=c({{ factors_color }}), distfun="{{ distance_fun }}", method="{{ agg_method }}", getProtlist = TRUE)
  capture.output(dev.off(), file=stderr())
  cat("<p><img src=\"{{ tmp_path }}/prot_heatmap.png\" /></p>")
  message("MCQREnd: heatmap")

{% elif mcqr_step == 8 %}
  message("MCQRBegin: protein_clustering")
  # Clustering the proteins
  cat("<h4>Protein clustering</h4>")
  # Cluster for the factor(s) of interest
  capture.output(XICAB_cluster <- mcq.compute.cluster(XICAB, flist=c("{{ factor_list }}"), nbclust={{ nb_cluster }}), file=stderr())

  # Display of the graph on the screen
  svglite("{{ tmp_path }}/prot_cluster.svg", width=14, height=12)
  mcq.plot.cluster(XICAB_cluster)
  capture.output(dev.off(), file=stderr())
  cat("<p><img src=\"{{ tmp_path }}/prot_cluster.svg\" /></p>")
  message("MCQREnd: protein_clustering")

{% elif mcqr_step == 9 %}

  message("MCQRBegin: anova")
  cat("<h3>Analyzing protein abundance variations</h3>")
  #****************************************************************************************************************
  # 3.3.9. Analyzing protein abundance variations
  #****************************************************************************************************************

  #############  Removing proteins showing little abundance variations. The selection criteria is the ratio between the minimal and the maximal mean abundance values computed for a factor or a combination of factors of interest.
  XICAB <- mcq.drop.low.fold.changes(XICAB, cutoff={{ fc_cutoff }}, flist=c({{ factors_list }}))

  ############# Differential analysis 
  cat("<h4>Differential analysis</h4>")
  # ANOVA for the factors of interest
  XICAB_ANOVA <- mcq.compute.anova(XICAB, flist=c({{ factors_list }}), inter={{ inter_bool }})

  # Display the distribution of the p-values for a factor of interest on the screen
  svglite("{{ tmp_path }}/anova.svg", width=14, height=12)
  mcq.plot.pvalues(XICAB_ANOVA)
  capture.output(dev.off(), file=stderr())
  cat("<p><img src=\"{{ tmp_path }}/anova.svg\" /></p>")
  message("MCQREnd: anova")

{% elif mcqr_step == 10 %}
  message("MCQRBegin: protein_abundance")
  cat("<h4>Tukey test</h4>")
  ############# Analyzing the proteins showing significant variations
  {% for factor in factors_list %}
    cat("<h4>Analyse the {{ factor }} factor</h4>")
    # Retrieving the proteins showing significant abundance variations for a given factor
    capture.output(XICAB_PROTEINS_selected_{{factor}} <- mcq.select.pvalues(XICAB_ANOVA, padjust={{ padjust }}, alpha={{ alpha }}, flist="{{ factor }}"), file=stderr())

    # Retrieving of the abundance data for the selected proteins
    capture.output(XICAB_selected_{{ factor }} <- mcq.select.from.protlist(XICAB, XICAB_PROTEINS_selected_{{ factor }}), file=stderr())
      
    # Tukey test
    # WARNING : Verify your packageVersion of agricolae (with the R function packageVersion("agricolae")), it must be the 1.2.9 version 
    #(the URL provide in the PAPPSO site it's an unstable version, waiting for the stable one)
    capture.output(XICAB_selected_tukey <- mcq.compute.tukey(XICAB_ANOVA, flist="{{ factor }}", protlist=XICAB_PROTEINS_selected_{{ factor }}), file=stderr())

    # Display of the graph on the screen
    svglite("{{ tmp_path }}/tukey_{{ factor }}_%01d.svg", width=14, height=12)
    capture.output(mcq.plot.tukey(XICAB_selected_tukey, qprot=XICAB), file=stderr())
    capture.output(dev.off(), file=stderr())
    
    nb_proteins = dim(XICAB_selected_{{ factor }}@proteins)[1]
    nb_pages = nb_proteins%/%4
    if(nb_proteins%%4 != 0) {
      nb_pages = nb_pages + 1
    }
    for(i in 1:nb_pages){
      cat(paste0("<p><img src=\"{{ tmp_path }}/tukey_{{ factor }}_", i, ".svg\" /></p>"))
    }
  {%endfor%}
  
  
  capture.output(union_list <- union({% for factor in factors_list%}
                        XICAB_PROTEINS_selected_{{factor}}
                        {%if factor != factors_list|last %}, {%endif%}
                      {% endfor %}), file=stderr())
                      
  capture.output(XICAB_selected <- mcq.select.from.protlist(XICAB, protlist = union_list), file=stderr())
  
  cat("<h3>Protein abundance</h3>") 
  ############# Plot of individual protein abundances
  nb_proteins <- length(XICAB_selected@proteins$protein)
  svglite("{{ tmp_path }}/prot_abundance%01d.svg", width=14, height=12)
  capture.output(mcq.plot.protein.abundance(XICAB_selected, factorToColor=c({{ factors_color }}), flist=c({{ factors_list_join }})), file=stderr())
  capture.output(dev.off(), file=stderr())
  
  nb_pages = nb_proteins%/%4
  if(nb_proteins%%4 != 0) {
    nb_pages = nb_pages + 1
  }
  for(i in 1:nb_pages){
    cat(paste0("<p><img src=\"{{ tmp_path }}/prot_abundance", i, ".svg\" /></p>"))
  }
  message("MCQREnd: protein_abundance")
{% elif mcqr_step == 11 %}

message("MCQRBegin: protein_fc_abundance")
  #Analyzing protein abundance ratios
  cat("<h3>Protein abundance ratios</h3>")
  # Computing ratios between two levels of a factor of interest
  capture.output(XICAB_RATIO <- mcq.compute.fold.change(XICAB_selected, flist="{{ fc_factor }}", numerator="{{ num }}", denominator="{{ denom }}"), file=stderr())

  svglite("{{ tmp_path }}/abundance_ratio.svg", width=14, height=12)
  capture.output(mcq.plot.fold.change(XICAB_RATIO), file=stderr())
  capture.output(dev.off(), file=stderr())
  cat("<p><img src=\"{{ tmp_path }}/abundance_ratio.svg\" /></p>")

  message("MCQREnd: protein_fc_abundance")
{% endif %}
