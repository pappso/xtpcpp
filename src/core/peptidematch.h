
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once
#include "peptideevidence.h"


class PeptideMatch
{
  public:
  PeptideMatch();
  PeptideMatch(const PeptideMatch &other);

  bool operator==(const PeptideMatch &other) const;

  PeptideMatch &operator=(const PeptideMatch &);


  /** @brief set start position of this peptide inside the protein sequence
   * @param start position in the protein amino acid sequence (starts at 0)
   * */
  void setStart(unsigned int start);
  /** @brief get start position of this peptide inside the protein sequence
   * @return start position in the protein amino acid sequence (starts at 0)
   * */
  unsigned int getStart() const;
  /** @brief get stop position of this peptide inside the protein sequence
   * @return stop position in the protein amino acid sequence (starts at 0)
   * */
  unsigned int getStop() const;

  /** @brief tells if this peptide contains a protein position
   * the position is the amino acid position on the protein sequence (starts
   * from 0)
   * */
  bool containsPosition(unsigned int position) const;

  void setPeptideEvidenceSp(PeptideEvidenceSp sp_peptide_evidence);
  const PeptideEvidence *getPeptideEvidence() const;
  PeptideEvidence *getPeptideEvidence();


  bool operator<(const PeptideMatch &r) const;

  private:
  unsigned int _start                  = 0;
  PeptideEvidence *_p_peptide_evidence = nullptr;
};
