/**
 * \file /core/tandem_run/tandembatchprocess.h
 * \date 5/9/2017
 * \author Olivier Langella
 * \brief handles execution of a bunch of X!Tandem process
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once
#include "tandemrunbatch.h"
#include "../../utils/workmonitor.h"
#include <QTemporaryFile>
#include <QXmlStreamWriter>
#include <QObject>
#include "../../gui/mainwindow.h"
#include <pappsomspp/processing/tandemwrapper/tandemwrapperrun.h>

class TandemBatchProcess : public QObject
{
  Q_OBJECT
  public:
  TandemBatchProcess(MainWindow *p_main_window,
                     WorkMonitorInterface *p_monitor,
                     const TandemRunBatch &tandem_run_batch);
  virtual ~TandemBatchProcess();

  virtual void run();

  protected:
  void writeXmlDatabaseFile(QXmlStreamWriter *p_out);
  void writeXmlInputFile(QXmlStreamWriter *p_out, const QString &mz_file);
  virtual void prepareXmlDatabaseFile();

  private:
  void runOne(const QString &mz_file);

  protected:
  QString _preset_file;
  TandemRunBatch _tandem_run_batch;
  WorkMonitorInterface *_p_monitor;
  QString _xml_database_file;
  int _max_xt_time_ms = (60000 * 60 * 24); // 1 day

  private:
  QTemporaryFile _tmp_database_file;
  MainWindow *_p_main_window = nullptr;
  QString m_currentMzFile;
};
