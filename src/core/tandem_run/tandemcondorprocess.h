/**
 * \file /core/tandem_run/tandemcondorprocess.h
 * \date 5/9/2017
 * \author Olivier Langella
 * \brief handles execution of a bunch of X!Tandem process through condor job
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "tandembatchprocess.h"
#include <QTemporaryDir>
#include "../condor_process/condorprocess.h"

class TandemCondorProcess : public TandemBatchProcess, public CondorProcess
{
  public:
  TandemCondorProcess(MainWindow *p_main_window,
                      WorkMonitorInterface *p_monitor,
                      const TandemRunBatch &tandem_run_batch);
  virtual ~TandemCondorProcess();

  virtual void prepareXmlDatabaseFile();
  virtual void run();

  private:
  unsigned int m_condorRequestMemory;
  unsigned int m_condorDiskUsage;
};
