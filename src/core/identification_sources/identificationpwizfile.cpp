/**
 * \filed core/identification_sources/identificationpwizfile.cpp
 * \date 17/6/2017
 * \author Olivier Langella
 * \brief read identification files with proteowizard library
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

//#include "../../input/identificationpwizreader.h"
#include "../project.h"
#include "identificationpwizfile.h"
#include <pappsomspp/exception/exceptionnotimplemented.h>

IdentificationPwizFile::IdentificationPwizFile(const QFileInfo &ident_file)
  : IdentificationDataSource(ident_file.absoluteFilePath()),
    _ident_file(ident_file)
{
  //_engine = IdentificationEngine::XTandem;
  m_identificationEngine = IdentificationEngine::peptider;
}

IdentificationPwizFile::IdentificationPwizFile(
  const IdentificationPwizFile &other)
  : IdentificationDataSource(other), _ident_file(other._ident_file)
{
  m_identificationEngine = other.m_identificationEngine;
}

IdentificationPwizFile::~IdentificationPwizFile()
{
}
void
IdentificationPwizFile::parseTo(Project *p_project [[maybe_unused]])
{
  qDebug();

  // IdentificationPwizReader pwiz_reader(_ident_file);
  throw pappso::ExceptionNotImplemented(
    QObject::tr("IdentificationPwizFile not implemented"));

  // MsRunSp msrun_sp =
  // p_project->getMsRunStore().getInstance(pwiz_reader.getMsrunName());

  //_engine = pwiz_reader.getIdentificationEngine();
  /*
  setMsRunSp(msrun_sp);
  std::vector<IdentificationGroup *> identification_list =
    p_project->getIdentificationGroupList();
  IdentificationGroup *identification_group_p = nullptr;
  if(p_project->getProjectMode() == ProjectMode::combined)
    {
      if(identification_list.size() == 0)
        {
          identification_group_p = p_project->newIdentificationGroup();
        }
      else
        {
          identification_group_p = identification_list[0];
        }
    }
  else
    {
      for(IdentificationGroup *identification_p_flist : identification_list)
        {
          if(identification_p_flist->containSample(
               msrun_sp.get()->getSampleName()))
            {
              identification_group_p = identification_p_flist;
              break;
            }
        }
      if(identification_group_p == nullptr)
        {
          identification_group_p = p_project->newIdentificationGroup();
        }
    }
  identification_group_p->addIdentificationDataSourceP(this);
  pwiz_reader.read(this, p_project, identification_group_p);
*/
  qDebug();
  //  throw pappso::PappsoException(QObject::tr("Error reading %1 XPIP file :\n
  //  %2").arg(_xtandem_file.absoluteFilePath()).arg(parser->errorString()));
}
