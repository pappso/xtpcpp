/**
 * \file /core/identification_sources/identificationmascotdatfile.h
 * \date 17/2/2018
 * \author Olivier Langella
 * \brief mascot dat identification file handler
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "identificationdatasource.h"
#include <QFileInfo>

class IdentificationMascotDatFile : public IdentificationDataSource
{
  public:
  IdentificationMascotDatFile(const QFileInfo &mascot_dat_file);
  IdentificationMascotDatFile(const IdentificationMascotDatFile &other);
  ~IdentificationMascotDatFile();

  virtual void parseTo(Project *p_project) override;

  // virtual const bool isValid(const PeptideEvidence * p_peptide_evidence,
  // const AutomaticFilterParameters & automatic_filter_parameters) const
  // override;

  private:
  const QFileInfo _mascot_dat_file;
};
