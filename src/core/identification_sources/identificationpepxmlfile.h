/**
 * \file /core/identification_sources/identificationpepxmlfile.h
 * \date 16/6/2018
 * \author Olivier Langella
 * \brief pep xml identification file handler
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "identificationdatasource.h"
#include <QFileInfo>

class IdentificationPepXmlFile : public IdentificationDataSource
{
  public:
  IdentificationPepXmlFile(const QFileInfo &mascot_dat_file);
  IdentificationPepXmlFile(const IdentificationPepXmlFile &other);
  ~IdentificationPepXmlFile();

  virtual void parseTo(Project *p_project) override;

  // virtual const bool isValid(const PeptideEvidence * p_peptide_evidence,
  // const AutomaticFilterParameters & automatic_filter_parameters) const
  // override;

  private:
  const QFileInfo _pep_xml_file;
};
