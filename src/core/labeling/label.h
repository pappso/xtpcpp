/**
 * \file core/label.h
 * \date 20/5/2017
 * \author Olivier Langella
 * \brief description of a label
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef LABEL_H
#define LABEL_H
#include <QString>
#include <vector>
#include <QDomNode>
#include <pappsomspp/amino_acid/aamodification.h>
#include <pappsomspp/peptide/peptide.h>
#include <QXmlStreamWriter>


struct LabelModification
{
  QString at;
  pappso::AaModificationP modification;
};


class Label
{
  public:
  Label(QDomNode &method_node);
  Label(const Label &other);
  bool containsAaModificationP(
    const std::list<pappso::AaModificationP> &modification_set) const;
  pappso::PeptideSp getLabeledPeptideSp(const pappso::Peptide *p_peptide) const;
  void writeMassChroqMl(QXmlStreamWriter *output_stream) const;
  const QString &getXmlId() const;

  const std::vector<LabelModification> &getLabelModifictionList() const;
  bool isEmpty() const;

  private:
  QString _xml_id;
  std::vector<LabelModification> _modification_list;
};

#endif // LABEL_H
