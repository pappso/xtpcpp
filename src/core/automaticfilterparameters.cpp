
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "automaticfilterparameters.h"

AutomaticFilterParameters::AutomaticFilterParameters()
{
}

AutomaticFilterParameters::AutomaticFilterParameters(
  const AutomaticFilterParameters &other)
{
  this->operator=(other);
}

AutomaticFilterParameters::~AutomaticFilterParameters()
{
}

AutomaticFilterParameters &
AutomaticFilterParameters::operator=(const AutomaticFilterParameters &other)
{
  _filter_minimum_peptide_evalue    = other._filter_minimum_peptide_evalue;
  _filter_minimum_protein_evalue    = other._filter_minimum_protein_evalue;
  _filter_minimum_peptide_per_match = other._filter_minimum_peptide_per_match;
  _filter_is_cross_sample_peptide_number =
    other._filter_is_cross_sample_peptide_number;
  m_filter_peptide_observed_in_less_samples_than =
    other.m_filter_peptide_observed_in_less_samples_than;
  m_filterFDR = other.m_filterFDR;

  return *this;
}


void
AutomaticFilterParameters::setFilterPeptideFDR(double fdr)
{
  m_filterFDR = fdr;
}
void
AutomaticFilterParameters::setFilterPeptideEvalue(pappso::pappso_double evalue)
{
  _filter_minimum_peptide_evalue = evalue;
}

void
AutomaticFilterParameters::setFilterProteinEvalue(pappso::pappso_double evalue)
{
  _filter_minimum_protein_evalue = evalue;
}
void
AutomaticFilterParameters::setFilterMinimumPeptidePerMatch(unsigned int number)
{
  _filter_minimum_peptide_per_match = number;
}
void
AutomaticFilterParameters::setFilterCrossSamplePeptideNumber(bool cross)
{
  _filter_is_cross_sample_peptide_number = cross;
}
double
AutomaticFilterParameters::getFilterPeptideFDR() const
{
  return (m_filterFDR);
}
pappso::pappso_double
AutomaticFilterParameters::getFilterPeptideEvalue() const
{
  return (_filter_minimum_peptide_evalue);
}
pappso::pappso_double
AutomaticFilterParameters::getFilterProteinEvalue() const
{
  return (_filter_minimum_protein_evalue);
}
unsigned int
AutomaticFilterParameters::getFilterMinimumPeptidePerMatch() const
{
  return (_filter_minimum_peptide_per_match);
}
bool
AutomaticFilterParameters::getFilterCrossSamplePeptideNumber() const
{
  return _filter_is_cross_sample_peptide_number;
}


void
AutomaticFilterParameters::setFilterPeptideObservedInLessSamplesThan(
  unsigned int number)
{
  m_filter_peptide_observed_in_less_samples_than = number;
}
unsigned int
AutomaticFilterParameters::getFilterPeptideObservedInLessSamplesThan() const
{
  return (m_filter_peptide_observed_in_less_samples_than);
}

bool
AutomaticFilterParameters::useFDR() const
{
  if(m_filterFDR != -1)
    {
      return true;
    }
  return false;
}
