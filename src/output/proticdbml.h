/**
 * \file output/proticdbml.h
 * \date 11/5/2017
 * \author Olivier Langella
 * \brief PROTICdbML writer
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QXmlStreamWriter>
#include <QFile>
#include <QString>
#include <QElapsedTimer>
#include <pappsomspp/amino_acid/aamodification.h>
#include "../core/project.h"
#include "../grouping/groupinggroup.h"

struct ProticPeptideHit;

class ProticdbMl
{
  public:
  ProticdbMl(const QString &out_filename);
  ~ProticdbMl();

  void write(ProjectSp sp_project);
  void close();

  private:
  void writeOboModif(pappso::AaModificationP mod);
  void writeCvParam(QString acc, QString value, QString description);
  void writeSequence(ProteinMatch *p_protein_match);
  void writeProject();
  void writeIdentMethod();
  void writeSamples();
  void writeMsRuns();
  void writeIdentificationRun(IdentificationGroup *p_identification);
  void writeIdentificationDataSource(
    IdentificationDataSource *p_identification_data_source);
  void writepeptideHits(IdentificationGroup *p_identification);
  void writepeptideHitsbyGroup(GroupingGroup *p_group);
  void writePeptideHit(QString xml_id, ProticPeptideHit &protic_peptide_hit);
  void writePtms(PeptideXtp *p_peptide);
  void writeMatchs(IdentificationGroup *p_identification);
  void writeMatch(std::vector<const ProteinMatch *> &protein_match_sg_list);

  const QString &getPeptideXmlId(const PeptideMatch *p_peptide_match) const;
  const QString &getProteinXmlId(const ProteinMatch *p_protein_match) const;

  private:
  QFile *_output_file;
  QXmlStreamWriter *_output_stream;
  ProjectSp _sp_project;
  IdentificationGroup *_p_identification_group;
  QElapsedTimer _duracel;
  std::map<QString, QString> _map_accession2xmlid;
  std::map<QString, QString> _sample_to_id;
  std::map<QString, QString> _peptidekey_to_id;
};
