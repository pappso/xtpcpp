/**
 * \file /output/mcqr/mcqrscpeptide.h
 * \date 28/3/2018
 * \author Olivier Langella
 * \brief write peptide sheet in MassChroqR format
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <odsstream/calcwriterinterface.h>
#include "../../core/project.h"
#include <pappsomspp/grouping/grppeptide.h>


struct ScPeptideLabel
{
  bool operator<(const ScPeptideLabel &other) const;

  const pappso::GrpPeptide *_p_grp_peptide = nullptr;
  const Label *_p_label                    = nullptr;
};

class McqRscPeptide
{
  public:
  McqRscPeptide(CalcWriterInterface *p_writer, const Project *p_project);

  void writeSheet();

  private:
  void writeIdentificationGroup(IdentificationGroup *p_ident);
  void writeHeaders(IdentificationGroup *p_ident);
  void writePeptidesInMsrun(
    const ProteinMatch *p_protein_match,
    const MsRun *p_msrun,
    const std::vector<PeptideMatch> &peptide_match_in_msrun,
    const std::map<ScPeptideLabel, std::size_t> &count_scan_zero);

  private:
  const Project *_p_project;
  CalcWriterInterface *_p_writer;
};
