/**
 * \file output/ods/odsexport.h
 * \date 11/4/2017
 * \author Olivier Langella
 * \brief ODS export
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "../../core/project.h"
#include <odsstream/calcwriterinterface.h>
#include "../../utils/workmonitor.h"

class OdsExport
{
  public:
  OdsExport(ProjectSp project);

  void write(CalcWriterInterface *p_writer, WorkMonitorInterface *p_monitor);

  void setEvenOrOddStyle(unsigned int number, CalcWriterInterface *p_writer);

  private:
  ProjectSp _p_project;
  OdsTableCellStyleRef _even_style;
  OdsTableCellStyleRef _odd_style;
};
