/**
 * \file output/ods/proteinsheet.cpp
 * \date 24/4/2017
 * \author Olivier Langella
 * \brief ODS protein sheet
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef PROTEINSHEET_H
#define PROTEINSHEET_H

#include "../../core/project.h"
#include <odsstream/calcwriterinterface.h>
#include "../../core/proteinmatch.h"
#include "odsexport.h"
#include "../../gui/protein_list_view/proteintablemodel.h"

class ProteinSheet
{
  public:
  ProteinSheet(OdsExport *p_ods_export,
               CalcWriterInterface *p_writer,
               const Project *p_project);

  private:
  void writeIdentificationGroup(IdentificationGroup *p_ident);
  void writeHeaders(IdentificationGroup *p_ident);
  void writeOneProtein(const GroupingGroup *p_group,
                       const ProteinMatch *p_protein_match);

  protected:
  void writeCellHeader(ProteinListColumn column);

  private:
  OdsExport *_p_ods_export;
  const Project *_p_project;
  CalcWriterInterface *_p_writer;
};


#endif // PROTEINSHEET_H
