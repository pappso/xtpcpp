/**
 * \file output/ods/comparspectrasheet.cpp
 * \date 30/4/2017
 * \author Olivier Langella
 * \brief ODS compar spectra sheet
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "comparspectrasheet.h"

#include <tuple>
#include <pappsomspp/utils.h>
#include <QDebug>


ComparSpectraSheet::ComparSpectraSheet(OdsExport *p_ods_export,
                                       CalcWriterInterface *p_writer,
                                       const Project *p_project,
                                       SubMonitor &sub_monitor)
  : ComparBaseSheet(p_ods_export, p_writer, p_project, sub_monitor)
{
  _title_sheet = "compar spectra";
}

void
ComparSpectraSheet::writeComparValue(const ProteinMatch *p_protein_match,
                                     ValidationState state,
                                     const MsRun *p_msrun,
                                     const Label *p_label)
{
  qDebug() << "ComparSpectraSheet::writeComparValue begin";
  _p_writer->writeCell(
    (std::size_t)p_protein_match->countSampleScan(state, p_msrun, p_label));
  qDebug() << "ComparSpectraSheet::writeComparValue end";
}


ComparSequenceSheet::ComparSequenceSheet(OdsExport *p_ods_export,
                                         CalcWriterInterface *p_writer,
                                         const Project *p_project,
                                         SubMonitor &sub_monitor)
  : ComparBaseSheet(p_ods_export, p_writer, p_project, sub_monitor)
{
  _title_sheet = "compar unique sequence";
}

void
ComparSequenceSheet::writeComparValue(const ProteinMatch *p_protein_match,
                                      ValidationState state,
                                      const MsRun *p_msrun,
                                      const Label *p_label)
{
  qDebug() << "ComparSequenceSheet::writeComparValue begin";
  _p_writer->writeCell(
    p_protein_match->countSequenceLi(state, p_msrun, p_label));
  qDebug() << "ComparSequenceSheet::writeComparValue end";
}

ComparPaiSheet::ComparPaiSheet(OdsExport *p_ods_export,
                               CalcWriterInterface *p_writer,
                               const Project *p_project,
                               SubMonitor &sub_monitor)
  : ComparBaseSheet(p_ods_export, p_writer, p_project, sub_monitor)
{
  _title_sheet = "compar PAI";
}

void
ComparPaiSheet::writeComparValue(const ProteinMatch *p_protein_match,
                                 ValidationState state [[maybe_unused]],
                                 const MsRun *p_msrun,
                                 const Label *p_label)
{
  qDebug() << "ComparPaiSheet::writeComparValue begin";
  _p_writer->writeCell(p_protein_match->getPAI(p_msrun, p_label));
  qDebug() << "ComparPaiSheet::writeComparValue end";
}

ComparEmpaiSheet::ComparEmpaiSheet(OdsExport *p_ods_export,
                                   CalcWriterInterface *p_writer,
                                   const Project *p_project,
                                   SubMonitor &sub_monitor)
  : ComparBaseSheet(p_ods_export, p_writer, p_project, sub_monitor)
{
  _title_sheet = "compar emPAI";
}

void
ComparEmpaiSheet::writeComparValue(const ProteinMatch *p_protein_match,
                                   ValidationState state [[maybe_unused]],
                                   const MsRun *p_msrun,
                                   const Label *p_label)
{
  qDebug() << "ComparEmpaiSheet::writeComparValue begin";
  _p_writer->writeCell(p_protein_match->getEmPAI(p_msrun, p_label));
  qDebug() << "ComparEmpaiSheet::writeComparValue end";
}

ComparNsafSheet::ComparNsafSheet(OdsExport *p_ods_export,
                                 CalcWriterInterface *p_writer,
                                 const Project *p_project,
                                 SubMonitor &sub_monitor)
  : ComparBaseSheet(p_ods_export, p_writer, p_project, sub_monitor)
{
  _title_sheet = "compar NSAF";
}

void
ComparNsafSheet::writeComparValue(const ProteinMatch *p_protein_match,
                                  ValidationState state [[maybe_unused]],
                                  const MsRun *p_msrun,
                                  const Label *p_label)
{
  qDebug() << "ComparNsafSheet::writeComparValue begin";
  pappso::pappso_double proto_nsaf_sum = 0;

  if(_label_list.size() > 0)
    {
      std::pair<std::map<QString, pappso::pappso_double>::iterator, bool> ret =
        _map_proto_nsaf_sum_by_msrun.insert(
          std::pair<QString, pappso::pappso_double>(
            QString("%1--%2").arg(p_msrun->getXmlId()).arg(p_label->getXmlId()),
            0));
      if(ret.second == false)
        {
          //"element 'z' already existed";
          proto_nsaf_sum = ret.first->second;
        }
      else
        {
          proto_nsaf_sum =
            _p_current_identification_group->computeProtoNsafSumFirstInSg(
              p_msrun, p_label);
          ret.first->second = proto_nsaf_sum;
        }

      _p_writer->writeCell(
        p_protein_match->getNsaf(proto_nsaf_sum, p_msrun, p_label));
    }
  else
    {
      std::pair<std::map<QString, pappso::pappso_double>::iterator, bool> ret =
        _map_proto_nsaf_sum_by_msrun.insert(
          std::pair<QString, pappso::pappso_double>(p_msrun->getXmlId(), 0));
      if(ret.second == false)
        {
          //"element 'z' already existed";
          proto_nsaf_sum = ret.first->second;
        }
      else
        {
          proto_nsaf_sum =
            _p_current_identification_group->computeProtoNsafSumFirstInSg(
              p_msrun);
          ret.first->second = proto_nsaf_sum;
        }

      _p_writer->writeCell(p_protein_match->getNsaf(proto_nsaf_sum, p_msrun));
    }
  qDebug() << "ComparNsafSheet::writeComparValue end";
}
