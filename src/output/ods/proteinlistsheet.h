/**
 * \file output/ods/proteinlistsheet.cpp
 * \date 05/07/2021
 * \author Olivier Langella
 * \brief ODS protein list for huge spectra output
 */

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../../core/project.h"
#include <odsstream/calcwriterinterface.h>
#include "../../core/proteinmatch.h"
#include "odsexport.h"
#include "../../gui/protein_list_view/proteintablemodel.h"

class ProteinListSheet
{
  public:
  ProteinListSheet(OdsExport *p_ods_export,
                   CalcWriterInterface *p_writer,
                   const Project *p_project,
                   SubMonitor &sub_monitor);

  private:
  void writeIdentificationGroup(IdentificationGroup *p_ident);
  void writeHeaders(IdentificationGroup *p_ident);
  void writeOneProtein(const GroupingGroup *p_group,
                       const ProteinMatch *p_protein_match);

  protected:
  void writeCellHeader(ProteinListColumn column);

  private:
  OdsExport *mp_odsExport;
  const Project *mp_project;
  CalcWriterInterface *mp_writer;
  SubMonitor &m_subMonitor;
};
