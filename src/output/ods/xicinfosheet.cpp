/**
 * \file /output/ods/xicinfosheet.cpp
 * \date 09/12/2020
 * \author Thomas Renne
 * \brief ODS XIC info sheet
 */

/*******************************************************************************
 * Copyright (c) 2020 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#include "xicinfosheet.h"

XicInfoSheet::XicInfoSheet(CalcWriterInterface *p_writer,
                           ProjectSp project_sp,
                           const std::vector<XicBox *> xic_boxs)
{
  mp_writer     = p_writer;
  mp_xicBoxList = xic_boxs;

  OdsTableSettings table_settings;
  table_settings.setVerticalSplit(1);
  mp_writer->setCurrentOdsTableSettings(table_settings);

  mp_writer->writeSheet(
    QString("%1_xic_info").arg(project_sp.get()->getProjectName()));
  writeHeaders();
  writeXicParamData(*(project_sp.get()->getMasschroqFileParametersSp().get()));
  for(XicBox *xic : mp_xicBoxList)
    {
      writeXicBoxData(xic);
    }
}

void
XicInfoSheet::writeHeaders()
{
  mp_writer->writeLine();
  mp_writer->setCellAnnotation("Information");
  mp_writer->writeCell("information");
  mp_writer->setCellAnnotation("Value");
  mp_writer->writeCell("Value");
}

void
XicInfoSheet::writeXicBoxData(XicBox *xic_box)
{
  mp_writer->writeLine();
  mp_writer->writeCell("msrun file");
  mp_writer->writeCell(xic_box->getMsRunSp()->getFileName());

  mp_writer->writeLine();
  mp_writer->writeCell("sample file");
  mp_writer->writeCell(xic_box->getMsRunSp()->getSampleName());

  mp_writer->writeLine();
  mp_writer->writeCell("sequence");
  mp_writer->writeCell(
    xic_box->getPeptideEvidenceP()->getPeptideXtpSp().get()->toString());

  mp_writer->writeLine();
  mp_writer->writeCell("charge");
  mp_writer->writeCell(
    QString::number(xic_box->getPeptideEvidenceP()->getCharge()));

  mp_writer->writeLine();
  mp_writer->writeCell("isotopic fraction distribution");
  mp_writer->writeCell(xic_box->getIsotopicFractionDistribution());
  mp_writer->writeLine();
}

void
XicInfoSheet::writeXicParamData(const MasschroqFileParameters &mcq_params)
{
  mp_writer->writeLine();
  mp_writer->writeCell("xic filters");
  if(mcq_params.msp_xicFilterSuiteString.get() == nullptr)
    {
      mp_writer->writeCell("");
    }
  else
    {
      mp_writer->writeCell(
        mcq_params.msp_xicFilterSuiteString.get()->toString());
    }

  mp_writer->writeLine();
  mp_writer->writeCell("smoothing");
  mp_writer->writeCell(
    QString::number(mcq_params.m_zivyParams._smoothing_half_window));

  mp_writer->writeLine();
  mp_writer->writeCell("minmax half window");
  mp_writer->writeCell(
    QString::number(mcq_params.m_zivyParams._minmax_half_window));

  mp_writer->writeLine();
  mp_writer->writeCell("maxmin half window");
  mp_writer->writeCell(
    QString::number(mcq_params.m_zivyParams._maxmin_half_window));

  mp_writer->writeLine();
  mp_writer->writeCell("minmax threshold");
  mp_writer->writeCell(
    QString::number(mcq_params.m_zivyParams._minmax_threshold));

  mp_writer->writeLine();
  mp_writer->writeCell("maxmin threshold");
  mp_writer->writeCell(
    QString::number(mcq_params.m_zivyParams._maxmin_threshold));

  mp_writer->writeLine();
}
