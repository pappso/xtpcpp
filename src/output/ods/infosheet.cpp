/**
 * \file output/ods/infosheet.cpp
 * \date 30/4/2017
 * \author Olivier Langella
 * \brief ODS info sheet
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "infosheet.h"
#include "../../config.h"
#include <QDebug>
#include <QSettings>


InfoSheet::InfoSheet(OdsExport *p_ods_export,
                     CalcWriterInterface *p_writer,
                     const Project *p_project)
  : _p_project(p_project)
{
  _p_ods_export = p_ods_export;
  _p_writer     = p_writer;
  p_writer->writeSheet("i2MassChroQ information");

  p_writer->writeCell("i2MassChroQ version :");
  p_writer->writeLine();
  p_writer->writeCell(QString("%1").arg(i2MassChroQ_VERSION));

  p_writer->writeLine();
  p_writer->writeLine();
  p_writer->writeCell("i2MassChroQ filters :");
  p_writer->writeLine();
  AutomaticFilterParameters filter_param =
    p_project->getAutomaticFilterParameters();

  p_writer->writeCell("Mode");
  if(p_project->getProjectMode() == ProjectMode::combined)
    {
      p_writer->writeCell("combine");
    }
  else
    {
      p_writer->writeCell("individual");
    }
  p_writer->writeLine();
  p_writer->writeCell("Maximum peptide Evalue");
  p_writer->writeCell(filter_param.getFilterPeptideEvalue());
  p_writer->writeLine();
  p_writer->writeCell("Minimum number of peptides per protein");
  p_writer->writeCell(
    (std::size_t)filter_param.getFilterMinimumPeptidePerMatch());
  p_writer->writeLine();
  p_writer->writeCell("Overall samples");
  p_writer->writeCell(filter_param.getFilterCrossSamplePeptideNumber());
  p_writer->writeLine();
  p_writer->writeCell("Maximum protein Evalue");
  p_writer->writeCell(filter_param.getFilterProteinEvalue());
  p_writer->writeLine();


  p_writer->writeLine();
  p_writer->writeLine();

  std::vector<const FastaFile *> conta_file_list =
    p_project->getProteinStore().getContaminantFastaFileList();
  if(conta_file_list.size() == 0)
    {
      p_writer->writeCell("contaminant pattern");
      p_writer->writeCell(
        p_project->getProteinStore().getRegexpContaminant().pattern());
    }
  else
    {
      p_writer->writeCell("contaminant fasta files");
      for(const FastaFile *fasta_file : conta_file_list)
        {
          p_writer->writeCell(fasta_file->getAbsoluteFilePath());
        }
    }
  p_writer->writeLine();

  std::vector<const FastaFile *> decoy_file_list =
    p_project->getProteinStore().getDecoyFastaFileList();
  if(conta_file_list.size() == 0)
    {
      p_writer->writeCell("decoy pattern");
      p_writer->writeCell(
        p_project->getProteinStore().getRegexpDecoy().pattern());
    }
  else
    {
      p_writer->writeCell("decoy fasta files");
      for(const FastaFile *fasta_file : decoy_file_list)
        {
          p_writer->writeCell(fasta_file->getAbsoluteFilePath());
        }
    }
  p_writer->writeLine();


  p_writer->writeLine();
  p_writer->writeLine();
  p_writer->writeCell("sheet definitions");
  p_writer->writeLine();
  p_writer->writeCell(QUrl("https://doi.org/10.1021/acs.jproteome.6b00632"),
                      "i2MassChroQ DOI link");
  p_writer->writeLine();
  QSettings settings;

  if(settings.value("export_ods/groups", "true").toBool())
    {
      p_writer->writeCell("groups");
      p_writer->writeCell(
        "simple statistics on the current grouping "
        "experiment (numbers of groups, subgroups...)");
      p_writer->writeLine();
    }

  if(settings.value("export_ods/proteins", "true").toBool())
    {
      p_writer->writeCell("proteins");
      p_writer->writeCell(
        "list of proteins after the protein inference computation");
      p_writer->writeLine();
    }
  if(settings.value("export_ods/peptides", "true").toBool())
    {
      p_writer->writeCell("peptides");
      p_writer->writeCell(
        "list of peptides used by the grouping algorithm. "
        "Beware : peptides are intended as unique pairs of "
        "sequence+mass, given that Leucine and Isoleucine as "
        "the same mass, all Leucines are considered as "
        "Isoleucines");
      p_writer->writeLine();
    }
  if(settings.value("export_ods/spectra", "true").toBool())
    {
      p_writer->writeCell("spectra");
      p_writer->writeCell(
        "list of all validated and checked spectrum match. "
        "Only the best assigned peptide (best Evalue) is "
        "reported ");
      p_writer->writeLine();
    }
  if(settings.value("export_ods/peptidepos", "true").toBool())
    {
      p_writer->writeCell("peptide pos");
      p_writer->writeCell(
        "list of all *real* peptides identified on their "
        "proteins, with their position on the sequence.");
      p_writer->writeLine();
    }
}
