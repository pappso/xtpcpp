message("UNIX non APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")
message("If building of the user manual is required, add -DBUILD_USER_MANUAL=1")
message("If using the locally built pappsomspp libs, add -DLOCAL_DEV=1")

# The TARGET changes according to the plaform
# For example, it changes to i2MassChroQ for macOS.
# Here we want it to be lowercase, UNIX-culture,
# and it is needed for the user manual build.
SET(TARGET i2masschroq)
SET(CAP_TARGET i2MassChroQ)

## Install directories
if(NOT CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX /usr)
endif()
set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/bin)
set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/share/doc/${TARGET})



find_package(QCustomPlot REQUIRED)
# Per instructions of the lib author:
# https://www.qcustomplot.com/index.php/tutorials/settingup
message(STATUS "Setting definition -DQCUSTOMPLOT_USE_LIBRARY.")
if(NOT TARGET QCustomPlot::QCustomPlot)
  add_library(QCustomPlot::QCustomPlot UNKNOWN IMPORTED)
  set_target_properties(QCustomPlot::QCustomPlot PROPERTIES
    IMPORTED_LOCATION             "${QCustomPlot_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${QCustomPlot_INCLUDE_DIR}"
    INTERFACE_COMPILE_DEFINITIONS QCUSTOMPLOT_USE_LIBRARY	)
endif()



if(NOT OdsStream_FOUND)
  find_package(OdsStream REQUIRED)
endif(NOT OdsStream_FOUND)


find_package(PappsoMSpp COMPONENTS Core Widget REQUIRED)

if(NOT RData_FOUND)
  find_package(RData REQUIRED)
endif(NOT RData_FOUND)

#sudo apt install libgrantlee5-dev
if(NOT Grantlee5_FOUND)
  find_package(Grantlee5 REQUIRED)
endif(NOT Grantlee5_FOUND)


find_package(ZLIB REQUIRED)
